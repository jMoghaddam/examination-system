package model;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.model.GridFSUploadOptions;
import com.mongodb.client.model.UpdateOptions;
import exceptions.DuplicatedStudentIdException;
import exceptions.DuplicatedUsernameException;
import logic.chat.Group;
import logic.chat.Member;
import logic.exam.Exam;
import logic.exam.answer.Answer;
import logic.exam.answer.DescriptiveAnswer;
import logic.exam.answer.MultiChoiceAnswer;
import logic.exam.answer.TrueFalseAnswer;
import logic.exam.question.*;
import logic.people.Participant;
import logic.people.Person;
import logic.people.Student;
import logic.people.Teacher;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.print.Doc;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class UserSetInfoHandler {

    MongoClient mongoClient = new MongoClient();
    MongoDatabase database = mongoClient.getDatabase("Examiner"); // database
    MongoCollection<Document> studentsDB = database.getCollection("Students"); // students collection
    MongoCollection<Document> teachersDB = database.getCollection("Teachers"); // teachers collection
    MongoCollection<Document> examsDB = database.getCollection("exams"); // exams collection
    GridFSBucket gridBucket = GridFSBuckets.create(database);
    RegisterHandler registerHandler= RegisterHandler.getInstance();

    // SignUp User
    public boolean setUserBasicInfo(Document form) {

        // check duplicated username
        Document document = new Document("username", form.get("username"));
        if (studentsDB.count(document) != 0 || teachersDB.count(document) != 0) {
            throw new DuplicatedUsernameException();
        }

        // check duplicated student id
        if (form.get("studentId") != null) {
            document = new Document("studentId", form.get("studentId"));
            if (studentsDB.count(document) != 0) {
                throw new DuplicatedStudentIdException();
            }
        }

        // If User is a Student
        if (form.get("studentId") != null) {
            try {
                studentsDB.insertOne(form);
            } catch (Exception e) {
                throw new DuplicatedUsernameException();
            }
        }

        // If User is a Teacher
        if (form.get("studentId") == null) {
            try {
                teachersDB.insertOne(form);
            } catch (Exception e) {
                throw new DuplicatedUsernameException();
            }
        }

        return true;

    }

    public ObjectId upload(String filePath, String fileName) {

        ObjectId fileId = null;
        try {
            // Create a gridFSBucket

            File f = new File(filePath);
            InputStream inStream = new FileInputStream(f);

            // Create some customize options for the details that describes
            // the uploaded image
            GridFSUploadOptions uploadOptions = new GridFSUploadOptions().chunkSizeBytes(1024).metadata(new Document("type", "image").append("content_type", "image/png"));

            fileId = gridBucket.uploadFromStream(fileName, inStream, uploadOptions);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fileId;

    }

    public void addExam(Person user, Exam exam) {

        Group group = new Group();
        Document examForm = makeExamForm(exam);
        String hash = UUID.randomUUID().toString();
        Document gdoc = new Document("hash", hash);
        group.setHash(hash);
        ArrayList<Document> members = new ArrayList<>();

        Member ostad = new Member();
        Document membership = new Document("id", user.getId().toString());
        ostad.setId(user.getId().toString());
        membership.append("status", 1);
        ostad.setStatus(1);
        group.getMembers().add(ostad);
        members.add(membership);

        for (Participant p : exam.getParticipants()) {
            Member mem = new Member();
            membership = new Document("id", p.getStudentId());
            mem.setId(p.getStudentId());
            membership.append("status", 0);
            mem.setStatus(0);
            members.add(membership);
            group.getMembers().add(mem);
        }
        gdoc.append("members", members);
        gdoc.append("messages", new ArrayList<>());
        examForm.append("group", gdoc);

        examsDB.insertOne(examForm);
        ObjectId eID = examsDB.find(examForm).first().getObjectId("_id");

        Document userFind = new Document("username", user.getUsername());
        Document fuk = new Document("exams", eID);
        Document update = new Document();
        update.append("$addToSet", fuk);
        teachersDB.updateOne(userFind, update);

        for (Participant par : exam.getParticipants()) {

            Document sForm = new Document("studentId", par.getStudentId());
            if (studentsDB.count(sForm) == 0) {
                Student st = new Student();
                st.setUsername(par.getStudentId());
                st.setPassword(par.getStudentId());
                st.setFirstName(par.getFirstName());
                st.setLastName(par.getLastName());
                st.setStudentId(par.getStudentId());
                registerHandler.signup(st);
            }

            Document sfuk = new Document("exams", eID);
            Document supdate = new Document();
            supdate.append("$addToSet", sfuk);
            studentsDB.updateOne(sForm, supdate);

        }

    }

    public void updateExam(Exam exam) {

        Document examForm = makeExamForm(exam);
        Document findExam = new Document("_id", exam.getId());
        Document update = new Document();
        update.append("$set", examForm);

        examsDB.updateOne(findExam, update);

        ObjectId eID = examsDB.find(examForm).first().getObjectId("_id");
        for (Participant par : exam.getParticipants()) {
            Document sForm = new Document("studentId", par.getStudentId());
            if (studentsDB.count(sForm) == 0) {
                Student st = new Student();
                st.setUsername(par.getStudentId());
                st.setPassword(par.getStudentId());
                st.setFirstName(par.getFirstName());
                st.setLastName(par.getLastName());
                st.setStudentId(par.getStudentId());
                registerHandler.signup(st);
            }

            Document sfuk = new Document("exams", eID);
            Document supdate = new Document();
            supdate.append("$addToSet", sfuk);
            studentsDB.updateOne(sForm, supdate);
        }

    }

    private Document makeExamForm(Exam exam) {
        Document examForm = new Document();
        examForm.append("title", exam.getTitle());
        examForm.append("reviewable", exam.isReviewable());
        examForm.append("startingDate", exam.getStartDate() + " " + exam.getStartTime() + ":00");
        if (exam.getDuration() != null) examForm.append("duration", exam.getDuration().toMillis());

        ArrayList<Document> adocs = new ArrayList<>();
        ArrayList<Document> qs = new ArrayList<>();
        for (Question question : exam.getQuestions()) {
            Document q = new Document();
            q.append("number", question.getNumber());
            q.append("title", question.getTitle());
            q.append("body", question.getBody());
            if (question.getDuration() != null) q.append("duration", question.getDuration().toMillis());
            else q.append("duration", null);
            q.append("point", question.getPoint());
            if (question instanceof DescriptiveQuestion) {
                DescriptiveQuestion desq = (DescriptiveQuestion) question;
                q.append("type", "descriptive");
                q.append("imageAllowed", desq.isImageAllowed());
                q.append("fileAllowed", desq.isFileAllowed());

                Document adoc = new Document();
                adoc.append("number", question.getNumber());
                adoc.append("grade", 0.0);
                adoc.append("type", "descriptive");
                adocs.add(adoc);
            }
            if (question instanceof TrueFalseQuestion) {
                TrueFalseQuestion desq = (TrueFalseQuestion) question;
                q.append("type", "true false");
                q.append("correctAnswer", desq.isCorrectAnswer());

                Document adoc = new Document();
                adoc.append("number", question.getNumber());
                adoc.append("grade", 0.0);
                adoc.append("type", "true false");
                adocs.add(adoc);
            }
            if (question instanceof MultiChoiceQuestion) {
                MultiChoiceQuestion desq = (MultiChoiceQuestion) question;
                q.append("type", "multi choice");
                Document correctAnswer = new Document();
                correctAnswer.append("number", desq.getCorrectAnswer().getNumber());
                correctAnswer.append("text", desq.getCorrectAnswer().getText());
                q.append("correctAnswer", correctAnswer);
                ArrayList<Document> answers = new ArrayList<>();
                for (Choice choice : desq.getChoices()) {
                    Document answer = new Document();
                    answer.append("number", choice.getNumber());
                    answer.append("text", choice.getText());
                    answers.add(answer);
                }
                q.append("choices", answers);

                Document adoc = new Document();
                adoc.append("number", question.getNumber());
                adoc.append("grade", 0.0);
                adoc.append("type", "multi choice");
                adocs.add(adoc);
            }
            qs.add(q);
        }

        examForm.append("questions", qs);

        int g = 0;
        ArrayList<Document> pars = new ArrayList<>();
        for (Participant participant : exam.getParticipants()) {
            Document par = new Document();
            par.append("studentId", participant.getStudentId());
            par.append("isBanned", participant.isBanned());
            if (participant.isBanned()) {
                g += exam.getQuestions().size();
            }
            par.append("hasParticipated", false);
            par.append("grade", null);
            par.append("rank", null);
            par.append("questionQueue", participant.getQuestionQueue());
            par.append("answers", adocs);
            pars.add(par);
        }
        examForm.append("participants", pars);

        Document result = new Document();
        result.append("graded", g);
        result.append("avg", 0.0);
        result.append("isSimple", 0);
        result.append("isNormal", 0);
        result.append("isHard", 0);
        examForm.append("results", result);

        return examForm;
    }

    public void setAnswer(Exam exam, Participant participant, boolean over) {

        ArrayList<Document> answers = new ArrayList<>();
        for (Answer a : participant.getAnswers()) {

            Document d = new Document();
            d.append("number", a.getNumber());
            d.append("grade", a.getGrade());

            if (a instanceof DescriptiveAnswer) {
                DescriptiveAnswer da = (DescriptiveAnswer) a;
                d.append("type", "descriptive");
                d.append("text", da.getText());
                if (da.getImage() != null) d.append("image", upload(da.getImage().getPath(), "answerImg"));
                if (da.getFile() != null) d.append("file", upload(da.getFile().getPath(), "answerFile"));
            }

            if (a instanceof TrueFalseAnswer) {
                TrueFalseAnswer da = (TrueFalseAnswer) a;
                d.append("type", "true false");
                d.append("answer", da.isAnswer());
            }

            if (a instanceof MultiChoiceAnswer) {
                MultiChoiceAnswer da = (MultiChoiceAnswer) a;
                Document c = new Document();
                d.append("type", "multi choice");
                if (da.getAnswer() != null) c.append("number", da.getAnswer().getNumber());
                if (da.getAnswer() != null) c.append("text", da.getAnswer().getText());
                d.append("answer", c);
            }

            answers.add(d);

        }


        Document findExam = new Document("_id", exam.getId());
        findExam.append("participants.studentId", participant.getStudentId());
        Document up = new Document("participants.$.answers", answers);
        up.append("participants.$.hasParticipated", true);
        up.append("results.graded", exam.getResult().getGraded());
        if (over) {
            up.append("participants.$.grade", participant.getGrade());
        }
        Document update = new Document("$set", up);
        examsDB.updateOne(findExam, update);

    }

    public void saveStats(Exam exam) {

        System.out.println(exam.getResult().getIsSimple());

        Document findExam = new Document("_id", exam.getId());
        Document up = new Document("results.isSimple", exam.getResult().getIsSimple());
        up.append("results.isNormal", exam.getResult().getIsNormal());
        up.append("results.isHard", exam.getResult().getIsHard());
        Document update = new Document("$set", up);
        examsDB.updateOne(findExam, update);

    }

    public void saveGraded(Exam exam, Answer answer, Participant participant) {

        Document findExam = new Document("_id", exam.getId());
        Document up = new Document("results.graded", exam.getResult().getGraded());
        Document update = new Document("$set", up);
        examsDB.updateOne(findExam, update);

        Document w = new Document("outer.studentId", participant.getStudentId());
        Document ww = new Document("inner.number", answer.getNumber());
        ArrayList<Document> arrFs = new ArrayList<>();
        arrFs.add(w);
        arrFs.add(ww);
        UpdateOptions updateOptions = new UpdateOptions().arrayFilters(arrFs);

        findExam = new Document("_id", exam.getId());
        up = new Document("participants.$[outer].answers.$[inner].grade", answer.getGrade());
        update = new Document("$set", up);
        examsDB.updateOne(findExam, update, updateOptions);

    }

    public void saveParticipantGrade(Exam exam, Participant participant) {

        Document findExam = new Document("_id", exam.getId());
        findExam.append("participants.studentId", participant.getStudentId());
        Document up = new Document("participants.$.grade", participant.getGrade());
        Document update = new Document("$set", up);
        examsDB.updateOne(findExam, update);

    }

    public void saveAvg(Exam exam) {

        Document findExam = new Document("_id", exam.getId());
        Document up = new Document("results.avg", exam.getResult().getAvg());
        Document update = new Document("$set", up);
        examsDB.updateOne(findExam, update);

    }

    public void saveRanks(Exam exam) {

        for (Participant participant : exam.getParticipants()) {

            Document findExam = new Document("_id", exam.getId());
            findExam.append("participants.studentId", participant.getStudentId());
            Document up = new Document("participants.$.rank", participant.getRank());
            Document update = new Document("$set", up);
            examsDB.updateOne(findExam, update);

        }

    }

    public void updateUser(Person user) {

        Document query = new Document("_id", user.getId());
        Document up = new Document("firstName", user.getFirstName());
        up.append("lastName", user.getLastName());
        up.append("bio", user.getBio());
        up.append("saveDirectoryPath", user.getSaveDirectoryPath());
        up.append("avatar", upload(user.getAvatar().getPath(), "avatar"));
        Document update = new Document("$set", up);

        if (user instanceof Teacher) teachersDB.updateOne(query, update);
        else studentsDB.updateOne(query, update);

    }
}
