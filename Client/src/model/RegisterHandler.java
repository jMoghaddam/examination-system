package model;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import exceptions.DuplicatedUsernameException;
import exceptions.WrongUsernameOrPasswordException;
import logic.people.Person;
import logic.people.Student;
import org.bson.Document;

import java.util.ArrayList;

// Connect to Database and Handle Login/Logout operations
public class RegisterHandler {

    // Singleton Pattern Instance
    private static final RegisterHandler instance = new RegisterHandler();

    private UserGetInfoHandler userGetInfoHandler = new UserGetInfoHandler();
    private UserSetInfoHandler userSetInfoHandler = new UserSetInfoHandler();

    MongoClient mongoClient = new MongoClient(); // mongodb
    MongoDatabase database = mongoClient.getDatabase("Examiner"); // database

    // Singleton Pattern Constructor
    private RegisterHandler() {}

    // get instance
    public static RegisterHandler getInstance(){
        return instance;
    }

    // Login user
    public Person login(String username, String password) throws WrongUsernameOrPasswordException {

        // Query
        Document loginForm = new Document();
        loginForm.append("username", username);
        loginForm.append("password", password);

        return userGetInfoHandler.getUserBasicInfo(loginForm);

    }

    // Signup a new user
    public boolean signup(Person person) throws DuplicatedUsernameException {

        // Query
        Document signupForm = new Document();
        signupForm.append("username", person.getUsername());
        signupForm.append("password", person.getPassword());
        signupForm.append("firstName", person.getFirstName());
        signupForm.append("lastName", person.getLastName());

        if (person instanceof Student) {
            Student student = (Student) person;
            signupForm.append("studentId", student.getStudentId());
        }

        if (person instanceof Student) signupForm.append("avatar", userSetInfoHandler.upload("src/assets/images/student.png", "avatar"));
        else signupForm.append("avatar", userSetInfoHandler.upload("src/assets/images/teacher.png", "avatar"));

        signupForm.append("bio", null);
        signupForm.append("saveDirectoryPath", System.getProperty("user.home") + "\\examiner");
        signupForm.append("exams", new ArrayList<>());

        return userSetInfoHandler.setUserBasicInfo(signupForm);

    }

}
