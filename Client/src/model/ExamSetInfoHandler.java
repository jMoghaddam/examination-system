package model;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import logic.exam.Exam;
import logic.people.Participant;
import org.bson.Document;

public class ExamSetInfoHandler {

    MongoClient mongoClient = new MongoClient();
    MongoDatabase database = mongoClient.getDatabase("Examiner"); // database
    MongoCollection<Document> studentsDB = database.getCollection("exams"); // students collection
    GridFSBucket gridBucket = GridFSBuckets.create(database);

}
