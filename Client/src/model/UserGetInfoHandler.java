package model;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import exceptions.WrongUsernameOrPasswordException;
import logic.Date;
import logic.Time;
import logic.chat.Group;
import logic.chat.Member;
import logic.chat.Message;
import logic.exam.Exam;
import logic.exam.answer.Answer;
import logic.exam.answer.DescriptiveAnswer;
import logic.exam.answer.MultiChoiceAnswer;
import logic.exam.answer.TrueFalseAnswer;
import logic.exam.question.*;
import logic.people.Participant;
import logic.people.Person;
import logic.people.Student;
import logic.people.Teacher;
import org.apache.poi.ss.formula.functions.T;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.print.Doc;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserGetInfoHandler {

    MongoClient mongoClient = new MongoClient();
    MongoDatabase database = mongoClient.getDatabase("Examiner"); // database
    MongoCollection<Document> studentsDB = database.getCollection("Students"); // students collection
    MongoCollection<Document> teachersDB = database.getCollection("Teachers"); // teachers collection
    MongoCollection<Document> examsDB = database.getCollection("exams"); // teachers collection
    MongoCollection<Document> membersDB = database.getCollection("members"); // members collection
    GridFSBucket gridBucket = GridFSBuckets.create(database);

    // User Login
    public Person getUserBasicInfo(Document form) {

        Document userInfo = null;
        Person user = null;

        // check what type user is
        userInfo = studentsDB.find(form).first();
        if (userInfo == null) {
            userInfo = teachersDB.find(form).first();
            if (userInfo == null) {
                throw new WrongUsernameOrPasswordException();
            } else {
                user = new Teacher();
            }
        } else {
            user = new Student();
        }

        // user set info
        user.setId(userInfo.getObjectId("_id"));
        user.setUsername(userInfo.getString("username"));
        user.setPassword(userInfo.getString("password"));
        user.setFirstName(userInfo.getString("firstName"));
        user.setLastName(userInfo.getString("lastName"));
        user.setBio(userInfo.getString("bio"));
        user.setSaveDirectoryPath(userInfo.getString("saveDirectoryPath"));
        if (user instanceof Student) ((Student) user).setStudentId(userInfo.getString("studentId"));
        user.setAvatar(getAvatar(userInfo.getObjectId("avatar"), user.getSaveDirectoryPath()));

        ArrayList<ObjectId> exams = (ArrayList<ObjectId>)userInfo.get("exams");
        ArrayList<Exam> examsx = new ArrayList<>();

        for (ObjectId examId : exams) {

            Document findExam = new Document("_id", examId);
            Document exam = examsDB.find(findExam).first();
            Exam e = new Exam();
            e.setId(findExam.getObjectId("_id"));
            e.setTitle(exam.getString("title"));
            e.setReviewable(exam.getBoolean("reviewable"));
            e.setStartDate(Date.toDate(exam.getString("startingDate").split(" ")[0]));
            e.setStartTime(Time.toDate(exam.getString("startingDate").split(" ")[1]));
            if (exam.get("duration") != null) e.setDuration(Time.toTime(exam.getLong("duration")));

            ArrayList<Document> qs = (ArrayList<Document>)exam.get("questions");
            ArrayList<Question> questions = new ArrayList<>();
            for (Document q : qs) {

                if (q.getString("type").equals("descriptive")) {
                    DescriptiveQuestion qu = new DescriptiveQuestion();
                    qu.setNumber(q.getInteger("number"));
                    qu.setTitle(q.getString("title"));
                    qu.setBody(q.getString("body"));
                    if (q.get("duration") != null) qu.setDuration(Time.sToTime(q.getLong("duration")));
                    qu.setPoint(q.getDouble("point"));
                    qu.setImageAllowed(q.getBoolean("imageAllowed"));
                    qu.setFileAllowed(q.getBoolean("fileAllowed"));
                    questions.add(qu);
                }

                if (q.getString("type").equals("true false")) {
                    TrueFalseQuestion qu = new TrueFalseQuestion();
                    qu.setNumber(q.getInteger("number"));
                    qu.setTitle(q.getString("title"));
                    qu.setBody(q.getString("body"));
                    if (q.get("duration") != null) qu.setDuration(Time.sToTime(q.getLong("duration")));
                    qu.setPoint(q.getDouble("point"));
                    qu.setCorrectAnswer(q.getBoolean("correctAnswer"));
                    questions.add(qu);
                }

                if (q.getString("type").equals("multi choice")) {
                    MultiChoiceQuestion qu = new MultiChoiceQuestion();
                    qu.setNumber(q.getInteger("number"));
                    qu.setTitle(q.getString("title"));
                    qu.setBody(q.getString("body"));
                    if (q.get("duration") != null) qu.setDuration(Time.sToTime(q.getLong("duration")));
                    qu.setPoint(q.getDouble("point"));

                    Document d = (Document) q.get("correctAnswer");
                    Choice ca = new Choice();
                    ca.setNumber(d.getInteger("number"));
                    ca.setText(d.getString("text"));
                    qu.setCorrectAnswer(ca);

                    ArrayList<Document> as = (ArrayList<Document>)q.get("choices");
                    ArrayList<Choice> cs = new ArrayList<>();
                    for (Document a : as) {
                        Choice c = new Choice();
                        c.setNumber(a.getInteger("number"));
                        c.setText(a.getString("text"));
                        cs.add(c);
                    }
                    qu.setChoices(cs);
                    questions.add(qu);
                }

            }
            e.setQuestions(questions);

            ArrayList<Document> ps = (ArrayList<Document>)exam.get("participants");
            ArrayList<Participant> participants = new ArrayList<>();
            for (Document p : ps) {
                Participant par = new Participant();

                Document findStudent = new Document("studentId", p.getString("studentId"));
                Document stu = studentsDB.find(findStudent).first();
                par.setStudentId(stu.getString("studentId"));
                par.setUsername(stu.getString("username"));
                par.setPassword(stu.getString("password"));
                par.setFirstName(stu.getString("firstName"));
                par.setLastName(stu.getString("lastName"));

                par.setBanned(p.getBoolean("isBanned"));
                par.setHasParticipated(p.getBoolean("hasParticipated"));
                if (p.getDouble("grade") != null) par.setGrade(p.getDouble("grade"));
                if (p.getInteger("rank") != null) par.setRank(p.getInteger("rank"));
                ArrayList<Integer> queue = (ArrayList<Integer>)p.get("questionQueue");
                par.setQuestionQueue(queue);

                ArrayList<Document> answers = (ArrayList<Document>)p.get("answers");
                ArrayList<Answer> as = new ArrayList<>();

                for (Document a : answers) {

                    if (a.getString("type").equals("descriptive")) {
                        DescriptiveAnswer ans = new DescriptiveAnswer();
                        ans.setNumber(a.getInteger("number"));
                        if (a.getDouble("grade") != null) ans.setGrade(a.getDouble("grade"));
                        else ans.setGrade(Double.NaN);
                        if (a.getString("text") != null) ans.setText(a.getString("text"));
                        if (a.getObjectId("image") != null) ans.setImage(getFile(a.getObjectId("image"), user.getSaveDirectoryPath(), "imgAnswer.jpg"));
                        if (a.getObjectId("file") != null) {
                            ans.setFile(getFile(a.getObjectId("file"), user.getSaveDirectoryPath(), "fileAnswer"));
                        }
                        as.add(ans);
                    }

                    if (a.getString("type").equals("true false")) {
                        TrueFalseAnswer ans = new TrueFalseAnswer();
                        ans.setNumber(a.getInteger("number"));
                        if (a.getDouble("grade") != null) ans.setGrade(a.getDouble("grade"));
                        else ans.setGrade(Double.NaN);
                        if (a.getBoolean("answer") != null) {
                            ans.setAnswer(a.getBoolean("answer"));
                            ans.setHasAnswered(true);
                        }
                        else ans.setHasAnswered(false);
                        as.add(ans);
                    }

                    if (a.getString("type").equals("multi choice")) {
                        MultiChoiceAnswer ans = new MultiChoiceAnswer();
                        ans.setNumber(a.getInteger("number"));
                        if (a.getDouble("grade") != null) ans.setGrade(a.getDouble("grade"));
                        else ans.setGrade(Double.NaN);
                        if (a.get("answer") != null) {
                            Document chDoc = (Document) a.get("answer");
                            Choice ch = new Choice();
                            ch.setNumber(chDoc.getInteger("number"));
                            ch.setText(chDoc.getString("text"));
                            ans.setAnswer(ch);
                        }
                        as.add(ans);
                    }

                }

                par.setAnswers(as);
                participants.add(par);
            }
            e.setParticipants(participants);

            Document result = (Document) exam.get("results");
            e.getResult().setGraded(result.getInteger("graded"));
            e.getResult().setAvg(result.getDouble("avg"));
            e.getResult().setIsSimple(result.getInteger("isSimple"));
            e.getResult().setIsNormal(result.getInteger("isNormal"));
            e.getResult().setIsHard(result.getInteger("isHard"));

            Document gdoc = (Document) exam.get("group");
            Group group = new Group();
            group.setHash(gdoc.getString("hash"));
            ArrayList<Document> ms = (ArrayList<Document>) gdoc.get("members");
            for (Document m : ms) {
                Member member = new Member();
                member.setId(m.getString("id"));
                member.setStatus(m.getInteger("status"));
                group.getMembers().add(member);
            }

            ArrayList<Document> msgs = (ArrayList<Document>) gdoc.get("messages");
            for (Document m : msgs) {
                Message message = new Message();
                message.setFrom(group.getMemberById(m.getString("from")));
                message.setTime(m.getString("time"));
                message.setText(m.getString("text"));
                group.getMessages().add(message);
            }
            e.setGroup(group);

            examsx.add(e);

        }
        user.setExams(examsx);

        return user;

    }

    // get files from database
    private File getFile(ObjectId id, String saveDirectory, String name) {

        String dir = saveDirectory + "\\" + name;

        try {
            FileOutputStream streamToDownloadTo = new FileOutputStream(dir);
            gridBucket.downloadToStream(id, streamToDownloadTo);
            streamToDownloadTo.close();
        } catch (IOException e) {
            // handle exception
        }

        return new File(dir);

    }

    // get avatars
    private String getAvatar(ObjectId id, String saveDirectory) {

        String dir = saveDirectory + "\\avatar.jpg";

        try {
            FileOutputStream streamToDownloadTo = new FileOutputStream(dir);
            gridBucket.downloadToStream(id, streamToDownloadTo);
            streamToDownloadTo.close();
        } catch (IOException e) {
            // handle exception
        }

        return dir;

    }

    public ArrayList<Exam> getTeacherExams(Person person) {

        Document form = new Document();
        form.append("username", person.getUsername());

        Document info = teachersDB.find(form).first();
        Document examsInfo = (Document) info.get("exams");

        ArrayList<Exam> exams = new ArrayList<>();

        if (examsInfo.isEmpty()) return exams;

        return null;

    }

    public void updateExam(Exam e, Person user) {

        System.out.println("####");
        Document query = new Document("_id", e.getId());
        Document exam = examsDB.find(query).first();

        e.setTitle(exam.getString("title"));
        e.setReviewable(exam.getBoolean("reviewable"));
        e.setStartDate(Date.toDate(exam.getString("startingDate").split(" ")[0]));
        e.setStartTime(Time.toDate(exam.getString("startingDate").split(" ")[1]));
        if (exam.get("duration") != null) e.setDuration(Time.toTime(exam.getLong("duration")));

        ArrayList<Document> qs = (ArrayList<Document>)exam.get("questions");
        ArrayList<Question> questions = new ArrayList<>();
        for (Document q : qs) {

            if (q.getString("type").equals("descriptive")) {
                DescriptiveQuestion qu = new DescriptiveQuestion();
                qu.setNumber(q.getInteger("number"));
                qu.setTitle(q.getString("title"));
                qu.setBody(q.getString("body"));
                if (q.get("duration") != null) qu.setDuration(Time.sToTime(q.getLong("duration")));
                qu.setPoint(q.getDouble("point"));
                qu.setImageAllowed(q.getBoolean("imageAllowed"));
                qu.setFileAllowed(q.getBoolean("fileAllowed"));
                questions.add(qu);
            }

            if (q.getString("type").equals("true false")) {
                TrueFalseQuestion qu = new TrueFalseQuestion();
                qu.setNumber(q.getInteger("number"));
                qu.setTitle(q.getString("title"));
                qu.setBody(q.getString("body"));
                if (q.get("duration") != null) qu.setDuration(Time.sToTime(q.getLong("duration")));
                qu.setPoint(q.getDouble("point"));
                qu.setCorrectAnswer(q.getBoolean("correctAnswer"));
                questions.add(qu);
            }

            if (q.getString("type").equals("multi choice")) {
                MultiChoiceQuestion qu = new MultiChoiceQuestion();
                qu.setNumber(q.getInteger("number"));
                qu.setTitle(q.getString("title"));
                qu.setBody(q.getString("body"));
                if (q.get("duration") != null) qu.setDuration(Time.sToTime(q.getLong("duration")));
                qu.setPoint(q.getDouble("point"));

                Document d = (Document) q.get("correctAnswer");
                Choice ca = new Choice();
                ca.setNumber(d.getInteger("number"));
                ca.setText(d.getString("text"));
                qu.setCorrectAnswer(ca);

                ArrayList<Document> as = (ArrayList<Document>)q.get("choices");
                ArrayList<Choice> cs = new ArrayList<>();
                for (Document a : as) {
                    Choice c = new Choice();
                    c.setNumber(a.getInteger("number"));
                    c.setText(a.getString("text"));
                    cs.add(c);
                }
                qu.setChoices(cs);
                questions.add(qu);
            }

        }
        e.setQuestions(questions);

        ArrayList<Document> ps = (ArrayList<Document>)exam.get("participants");
        ArrayList<Participant> participants = new ArrayList<>();
        for (Document p : ps) {
            Participant par = new Participant();

            Document findStudent = new Document("studentId", p.getString("studentId"));
            Document stu = studentsDB.find(findStudent).first();
            par.setStudentId(stu.getString("studentId"));
            par.setUsername(stu.getString("username"));
            par.setPassword(stu.getString("password"));
            par.setFirstName(stu.getString("firstName"));
            par.setLastName(stu.getString("lastName"));

            par.setBanned(p.getBoolean("isBanned"));
            par.setHasParticipated(p.getBoolean("hasParticipated"));
            if (p.getDouble("grade") != null) par.setGrade(p.getDouble("grade"));
            if (p.getInteger("rank") != null) par.setRank(p.getInteger("rank"));
            ArrayList<Integer> queue = (ArrayList<Integer>)p.get("questionQueue");
            par.setQuestionQueue(queue);

            ArrayList<Document> answers = (ArrayList<Document>)p.get("answers");
            ArrayList<Answer> as = new ArrayList<>();

            for (Document a : answers) {

                if (a.getString("type").equals("descriptive")) {
                    DescriptiveAnswer ans = new DescriptiveAnswer();
                    ans.setNumber(a.getInteger("number"));
                    if (a.getDouble("grade") != null) ans.setGrade(a.getDouble("grade"));
                    else ans.setGrade(Double.NaN);
                    if (a.getString("text") != null) ans.setText(a.getString("text"));
                    if (a.getObjectId("image") != null) ans.setImage(getFile(a.getObjectId("image"), user.getSaveDirectoryPath(), "imgAnswer.jpg"));
                    if (a.getObjectId("file") != null) ans.setFile(getFile(a.getObjectId("file"), user.getSaveDirectoryPath(), "fileAnswer.jpg"));
                    as.add(ans);
                }

                if (a.getString("type").equals("true false")) {
                    TrueFalseAnswer ans = new TrueFalseAnswer();
                    ans.setNumber(a.getInteger("number"));
                    if (a.getDouble("grade") != null) ans.setGrade(a.getDouble("grade"));
                    else ans.setGrade(Double.NaN);
                    if (a.getBoolean("answer") != null) {
                        ans.setAnswer(a.getBoolean("answer"));
                        ans.setHasAnswered(true);
                    }
                    else ans.setHasAnswered(false);
                    as.add(ans);
                }

                if (a.getString("type").equals("multi choice")) {
                    MultiChoiceAnswer ans = new MultiChoiceAnswer();
                    ans.setNumber(a.getInteger("number"));
                    if (a.getDouble("grade") != null) ans.setGrade(a.getDouble("grade"));
                    else ans.setGrade(Double.NaN);
                    if (a.get("answer") != null) {
                        Document chDoc = (Document) a.get("answer");
                        Choice ch = new Choice();
                        ch.setNumber(chDoc.getInteger("number"));
                        ch.setText(chDoc.getString("text"));
                        ans.setAnswer(ch);
                    }
                    as.add(ans);
                }

            }

            par.setAnswers(as);
            participants.add(par);
        }
        e.setParticipants(participants);

        Document result = (Document) exam.get("results");
        e.getResult().setGraded(result.getInteger("graded"));
        e.getResult().setAvg(result.getDouble("avg"));
        e.getResult().setIsSimple(result.getInteger("isSimple"));
        e.getResult().setIsNormal(result.getInteger("isNormal"));
        e.getResult().setIsHard(result.getInteger("isHard"));

    }

    public Person getTeacher(String id) {

        Document query = new Document("_id", new ObjectId(id));
        Document form = teachersDB.find(query).first();
        Teacher teacher = new Teacher();
        teacher.setFirstName(form.getString("firstName"));
        teacher.setLastName(form.getString("lastName"));

        return teacher;
    }
}
