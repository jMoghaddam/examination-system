import chat.ChatClient;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import utils.ScreenController;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {

        ChatClient client = new ChatClient("localhost", 8083);
        client.connect();

        // Managing Different Screens and the transitions
        ScreenController sc = new ScreenController();
        sc.setClient(client);

        // Set Initial Screen
        sc.setLoginScreen();

        // Stage Properties
        Scene scene = new Scene(sc);
        primaryStage.setScene(scene);
        primaryStage.setFullScreen(true);
        primaryStage.setFullScreenExitHint("");
        primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
