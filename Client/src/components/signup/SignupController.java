package components.signup;

import components.signup.states.State1;
import components.signup.states.State2;
import components.signup.states.State3;
import exceptions.DuplicatedStudentIdException;
import exceptions.DuplicatedUsernameException;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import logic.people.Person;
import logic.people.Student;
import logic.people.Teacher;
import model.RegisterHandler;
import utils.ControlledScreen;
import utils.ScreenController;

import java.io.IOException;

public class SignupController implements ControlledScreen {

    // UI Elements
    public Button nextBtn;
    public VBox contentLayout;
    public Rectangle square1;
    public Rectangle square2;
    public Rectangle square3;
    private HBox layout1;
    private VBox layout2;
    private VBox layout3;
    public Label dialog;

    // Subclasses Constructors
    private State1 state1;
    private State2 state2;
    private State3 state3;

    private ScreenController sc; // Screen Transition
    private RegisterHandler rh = RegisterHandler.getInstance(); // Signup
    private int state = 1; // state manager
    private static int counter = 0; // state manager
    private Person user = null; // user document

    @FXML
    public void initialize() {
        state = 1;
        renderState1();
        dialog.setVisible(false);
    }

    @Override
    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }

    // next button clicked
    public void step(ActionEvent actionEvent) {
        if (state == 1) {

            state = 2;
            if (state1.type.equals("student")) user = new Student();
            else user = new Teacher();
            renderState2(state1.type);

        } else if (state == 2) {

            if (!(state2.firstname.getText().equals("") || state2.lastname.getText().equals(""))) {
                if (!(state1.type.equals("student") && state2.studentid.getText().equals(""))) {
                    state = 3;
                    renderState3();
                } else {
                    alert("فیلدها را پر کنید");
                }
            } else {
                alert("فیلدها را پر کنید");
            }

        } else {

            if (!(state3.username.getText().equals("") || state3.password.getText().equals(""))) {

                if (!(state3.username.getText().contains(" ") || state3.password.getText().contains(" "))) {
                    user.setUsername(state3.username.getText());
                    user.setPassword(state3.password.getText());
                    user.setFirstName(state2.firstname.getText());
                    user.setLastName(state2.lastname.getText());
                    if (user instanceof Student) ((Student) user).setStudentId(state2.studentid.getText());
                    user.setSaveDirectoryPath(System.getProperty("user.home") + "/examiner");
                    signup();
                } else {
                    alert("نام کاربری نامعتبر");
                }

            } else {
                alert("فیلد هارا پر کنید");
            }
        }

    }

    // signup
    private void signup() {

        try {

            rh.signup(user);
            sc.setLoginScreen();
            state = 1;
            renderState1();
            contentLayout.getChildren().removeAll(layout3);
            square1.setFill(Color.BLACK);
            square3.setFill(Color.WHITE);
            square3.setStroke(Color.BLACK);

        } catch (DuplicatedUsernameException e1) {
            alert("نام کاربری قبلا انتخاب شده است");
        } catch (DuplicatedStudentIdException e2) {
            alert("این شماره دانشجویی قبلا ثبت شده است");
        }

    }

    // signup state 3 - get user account info
    private void renderState3() {
        nextBtn.setText("ثبت نام");
        contentLayout.getChildren().removeAll(layout2);

        square3.setFill(Color.BLACK);
        square2.setFill(Color.WHITE);
        square2.setStroke(Color.BLACK);

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("states/state3.fxml"));
            layout3 = loader.load();
            state3 = loader.getController();
            int btnIndex = contentLayout.getChildren().indexOf(nextBtn);
            contentLayout.getChildren().add(btnIndex, layout3);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // signup state 2 - get user personal info
    private void renderState2(String type) {
        contentLayout.getChildren().removeAll(layout1);

        square2.setFill(Color.BLACK);
        square1.setFill(Color.WHITE);
        square1.setStroke(Color.BLACK);

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("states/state2.fxml"));
            layout2 = loader.load();
            state2 = loader.getController();
            if (type.equals("teacher")) state2.setTypeTeacher();
            int btnIndex = contentLayout.getChildren().indexOf(nextBtn);
            contentLayout.getChildren().add(btnIndex, layout2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // signup state 1 - get type of user
    public void renderState1() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("states/state1.fxml"));
            layout1 = loader.load();
            state1 = loader.getController();
            int btnIndex = contentLayout.getChildren().indexOf(nextBtn);
            contentLayout.getChildren().add(btnIndex, layout1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // toast message
    private void alert(String message) {

        // Set Toast Message
        dialog.setText(message);

        // Fade in Fade out Animation
        FadeTransition fadeIn = new FadeTransition(
                Duration.millis(2000)
        );
        fadeIn.setNode(dialog);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.setCycleCount(2);
        fadeIn.setAutoReverse(true);
        fadeIn.setOnFinished(actionEvent -> dialog.setVisible(false));

        if (!dialog.isVisible()) {
            dialog.setVisible(true);
            fadeIn.playFromStart();
        }

    }

    public void exit(MouseEvent mouseEvent) {
        sc.setLoginScreen();
    }
}
