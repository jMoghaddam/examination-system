package components.signup.states;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

public class State1 {

    // UI Elements
    public VBox studentCard;
    public VBox teacherCard;

    // state manager
    public String type = "student";

    @FXML
    public void initialize() {

        studentCard.setOnMouseClicked((e) -> {
            studentCard.setStyle("-fx-border-color: #1BA098;" + "-fx-border-width: 3");
            teacherCard.setStyle("-fx-border-color: #fff;" + "-fx-border-width: 3");
            type = "student";
        });

        teacherCard.setOnMouseClicked((e) -> {
            studentCard.setStyle("-fx-border-color: #fff;" + "-fx-border-width: 3");
            teacherCard.setStyle("-fx-border-color: #1BA098;" + "-fx-border-width: 3");
            type = "teacher";
        });

    }

}
