package components.signup.states;

import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class State2 {

    // UI Elements
    public VBox inputs;
    public TextField firstname;
    public TextField lastname;
    public TextField studentid;

    // if user is a teacher
    public void setTypeTeacher() {
        inputs.getChildren().removeAll(studentid);
    }

}
