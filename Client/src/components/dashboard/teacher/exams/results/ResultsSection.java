package components.dashboard.teacher.exams.results;

import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import logic.exam.Exam;
import logic.people.Participant;
import logic.people.Teacher;
import utils.ExcelHandler;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ResultsSection {

    public BarChart bar;
    public Label title;
    public Label avg;
    public Button excel;
    private ArrayList<Exam> exams;
    XYChart.Series<String,Float> series = new XYChart.Series<>();
    private ExcelHandler excelHandler = new ExcelHandler();
    private Teacher user;

    public void setExams(ArrayList<Exam> exams) {
        this.exams = exams;

        excel.setOnMouseClicked((e) -> {
            File f = excelHandler.generateExamsResult(exams, user);
            try {
                Desktop.getDesktop().open(f);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        for (Exam e : exams) {
            if (e.getResult().getGraded() == e.getParticipants().size() * e.getQuestions().size()) {
                series.getData().add(new XYChart.Data(e.getTitle(), e.getResult().getAvg()));
            }
        }

        bar.getData().add(series);

        for (XYChart.Data<String, Float> dt : series.getData()) {
            final Node n = dt.getNode();
            n.setOnMouseClicked((e) -> {
                Exam ex = getExamById(dt.getXValue());
                title.setText(ex.getTitle());
                avg.setText(String.valueOf(ex.getResult().getAvg()));
            });
        }
    }

    private Exam getExamById(String id) {
        for (Exam exam : exams){
            if (exam.getTitle().equals(id)) return exam;
        }

        return null;
    }

    public void setUser(Teacher user) {
        this.user = user;
    }
}
