package components.dashboard.teacher.exams.results;

import components.dashboard.teacher.TeacherDashboard;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.exam.Exam;
import logic.people.Participant;
import logic.people.Teacher;
import utils.ExcelHandler;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class ExamResults {

    public BarChart bar;
    public VBox info;
    public Label name;
    public HBox subgrades;
    public Label grade;
    public BarChart vote;
    public Button excel;
    public Label date;
    public Label time;
    public Label duration;
    public Label review;
    public Label title;
    public Label avg;
    private TeacherDashboard td;
    private Exam exam;
    private ExcelHandler excelHandler = new ExcelHandler();
    XYChart.Series<String,Float> series = new XYChart.Series<>();
    XYChart.Series<String,Integer> votes = new XYChart.Series<>();
    private Teacher user;

    public void setTd(TeacherDashboard td) {
        this.td = td;
    }

    public void setExam(Exam exam) {
        this.exam = exam;

        title.setText(exam.getTitle());

        if (exam.isReviewable()) {
            review.setStyle("-fx-background-color: #DEB992");
            review.setTextFill(Color.valueOf("#fff"));
        } else {
            review.setStyle("-fx-border-color: #DEB992");
            review.setTextFill(Color.valueOf("#DEB992"));
        }
        avg.setText(String.valueOf(exam.getResult().getAvg()));

        if (exam.getDuration() != null) duration.setText(exam.getDuration().toString());
        time.setText(exam.getStartTime().toString());
        date.setText(exam.getStartDate().toString());

        excel.setOnMouseClicked((e) -> {

            File file = excelHandler.generateExamResult(exam, user);
            try {
                Desktop.getDesktop().open(file);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

        });

        votes.getData().add(new XYChart.Data("اسون", exam.getResult().getIsSimple()));
        votes.getData().add(new XYChart.Data("متوسط", exam.getResult().getIsNormal()));
        votes.getData().add(new XYChart.Data("سخت", exam.getResult().getIsHard()));

        vote.getData().add(votes);

        for (Participant p : exam.getParticipants()) {
            if (!Double.isNaN(p.getGrade())) {
                series.getData().add(new XYChart.Data(p.getStudentId(),p.getGrade()));
            }
        }

        bar.getData().add(series);

        for (XYChart.Data<String, Float> dt : series.getData()) {
            final Node n = dt.getNode();
            n.setOnMouseClicked((e) -> {
                Participant p = getParticipantById(dt.getXValue());
                name.setText(p.getFirstName() + " " + p.getLastName());
                grade.setText(String.valueOf(p.getGrade()));
            });
        }

    }

    private Participant getParticipantById(String id) {

        for (Participant p : exam.getParticipants()){
            if (p.getStudentId().equals(id)) return p;
        }

        return null;

    }

    public void setUser(Teacher user) {
        this.user = user;
    }
}
