package components.dashboard.teacher.exams.examInfo.question;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import logic.exam.question.Question;
import logic.exam.question.TrueFalseQuestion;

public class AddTrueFalseQuestion {

    public Label wrong;
    public Label ok;

    private Question question;
    public boolean answerKey = true;

    @FXML
    public void initialize() {

        ok.setOnMouseClicked((e) -> toggleOk(true));
        wrong.setOnMouseClicked((e) -> toggleOk(false));

    }

    public void setQuestion(Question question) {
        this.question = question;

        if (question != null) {
            TrueFalseQuestion holder = (TrueFalseQuestion) question;
            if (!holder.isCorrectAnswer()) toggleOk(false);

            ok.setOnMouseClicked(null);
            wrong.setOnMouseClicked(null);
        }

    }

    private void toggleOk(boolean key) {
        answerKey = key;

        if (answerKey) {
            ok.setTextFill(Color.valueOf("#DEB992"));
            wrong.setTextFill(Color.valueOf("#707070"));
        } else {
            wrong.setTextFill(Color.valueOf("#DEB992"));
            ok.setTextFill(Color.valueOf("#707070"));
        }
    }
}
