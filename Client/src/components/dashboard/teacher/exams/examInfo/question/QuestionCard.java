package components.dashboard.teacher.exams.examInfo.question;

import components.dashboard.teacher.exams.examInfo.ExamInfo;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import logic.exam.question.DescriptiveQuestion;
import logic.exam.question.MultiChoiceQuestion;
import logic.exam.question.Question;
import logic.exam.question.TrueFalseQuestion;

import java.io.File;

public class QuestionCard {

    public Label grade;
    public Label duration;
    public ImageView body;
    public Label type;
    public Label title;
    private Question question;
    private ExamInfo examInfo;

    public void setInfo(Question question) {
        this.question = question;
        grade.setText(String.valueOf(question.getPoint()));
        if (question.getDuration() != null) duration.setText(question.getDuration().toString());
        else duration.setText("N/A");

        File file = new File("src/assets/images/question.png");
        Image image = new Image(file.toURI().toString());
        body.setImage(image);

        if (question instanceof DescriptiveQuestion) type.setText("تشریحی");
        if (question instanceof TrueFalseQuestion) type.setText("صحیح/غلط");
        if (question instanceof MultiChoiceQuestion) type.setText("تستی");

        title.setText(question.getTitle());

        body.setOnMouseClicked((e) -> {
            examInfo.renderAddQuestion(question);
        });

    }

    public void setExamInfo(ExamInfo examInfo) {
        this.examInfo = examInfo;
    }
}
