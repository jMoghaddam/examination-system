package components.dashboard.teacher.exams.examInfo.question;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import logic.exam.question.DescriptiveQuestion;
import logic.exam.question.Question;

public class AddDescriptiveQuestion {

    public Label fileAllowed;
    public Label imageAllowed;

    public boolean image = true;
    public boolean file = false;
    private Question question;

    @FXML
    public void initialize() {

        imageAllowed.setOnMouseClicked((e) -> toggleImageAllowed());
        fileAllowed.setOnMouseClicked((e) -> toggleFileAllowed());

    }

    public void setQuestion(Question question) {
        this.question = question;

        if (question != null) {
            DescriptiveQuestion holder = (DescriptiveQuestion) question;
            if (!holder.isImageAllowed()) toggleImageAllowed();
            if (holder.isFileAllowed()) toggleFileAllowed();

            imageAllowed.setOnMouseClicked(null);
            fileAllowed.setOnMouseClicked(null);
        }
    }

    private void toggleImageAllowed() {

        if (image) {
            imageAllowed.setTextFill(Color.valueOf("#707070"));
            image = false;
        } else {
            imageAllowed.setTextFill(Color.valueOf("#DEB992"));
            image = true;
        }

    }

    private void toggleFileAllowed() {

        if (file) {
            fileAllowed.setTextFill(Color.valueOf("#707070"));
            file = false;
        } else {
            fileAllowed.setTextFill(Color.valueOf("#DEB992"));
            file = true;
        }

    }

}
