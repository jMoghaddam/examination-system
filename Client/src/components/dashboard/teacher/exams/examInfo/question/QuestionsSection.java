package components.dashboard.teacher.exams.examInfo.question;

import components.dashboard.teacher.exams.examInfo.ExamInfo;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import logic.exam.Exam;
import logic.exam.question.Question;
import logic.people.Participant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class QuestionsSection {

    public VBox contentLayout;
    public ImageView add;
    private ArrayList<Question> questions;
    private HashMap<Integer, HBox> layouts = new HashMap<>();
    private HashMap<Integer, QuestionCard> controllers = new HashMap<>();
    private ExamInfo examInfo;
    private Exam exam;

    private int counter = 0;

    @FXML
    public void initialize() {

        add.setOnMouseClicked((e) -> {
            examInfo.renderAddQuestion(null);
        });

        add.setCursor(Cursor.HAND);

    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
        renderQuestions();
    }

    public void renderQuestions() {

        for (Question question : questions) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("questionCard.fxml"));
                HBox holderLayout = loader.load();
                QuestionCard holderController = loader.getController();
                holderController.setExamInfo(examInfo);
                holderController.setInfo(question);
                controllers.put(counter, holderController);
                contentLayout.getChildren().add(holderLayout);
                layouts.put(counter, holderLayout);
            } catch (IOException e) {
                e.printStackTrace();
            }

            counter++;

        }

    }

    public void setExamInfo(ExamInfo examInfo, boolean outdated) {
        this.examInfo = examInfo;

        if (outdated) {
            contentLayout.getChildren().removeAll(add);
        }
    }

    public void submit(Question question) {

        question.setNumber(counter + 1);
        questions.add(question);

        for (Participant p : exam.getParticipants()) {
            if (!p.getQuestionQueue().contains(question.getNumber())) {
                p.getQuestionQueue().add(question.getNumber());
            }
        }

        examInfo.renderQuestions();
        examInfo.renderParticipants();

    }

    public void update() {
        examInfo.renderQuestions();
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }
}
