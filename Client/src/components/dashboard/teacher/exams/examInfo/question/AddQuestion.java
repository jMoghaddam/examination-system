package components.dashboard.teacher.exams.examInfo.question;

import components.dashboard.teacher.exams.examInfo.ExamInfo;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.Time;
import logic.exam.Exam;
import logic.exam.question.*;

import java.io.IOException;
import java.util.ArrayList;

public class AddQuestion {

    public Label multiChoice;
    public Label trueFalse;
    public Label descriptive;
    public TextField name;
    public HBox contentLayout;
    public Button cancel;
    public Button apply;
    public TextArea body;
    public TextField point;
    public TextField duration;
    private HBox addDescriptiveQuestionLayout;
    private HBox addTrueFalseQuestionLayout;
    private ScrollPane addMultiChoiceQuestionLayout;
    private AddDescriptiveQuestion addDescriptiveQuestion;
    private AddTrueFalseQuestion addTrueFalseQuestion;
    private AddMultiChoiceQuestion addMultiChoiceQuestion;

    private ExamInfo examInfo;
    private QuestionsSection questionsSection;
    private Exam exam;
    private Question question;
    private String state = "descriptive";
    private boolean outdated;

    @FXML
    public void initialize() {

        descriptive.setOnMouseClicked((e) -> {
            renderDescriptive();
        });

        trueFalse.setOnMouseClicked((e) -> {
            renderTrueFalse();
        });

        multiChoice.setOnMouseClicked((e) -> {
            renderMultiChoice();
        });

        cancel.setOnMouseClicked((e) -> examInfo.renderQuestions());

        apply.setOnMouseClicked((e) -> {

            if (!outdated && question != null) {

                if (checkStatus()) {
                    if (state.equals("descriptive")) {

                        DescriptiveQuestion q = (DescriptiveQuestion) question;
                        q.setTitle(name.getText());
                        q.setBody(body.getText());
                        q.setDuration(Time.toDate(duration.getText()));
                        q.setPoint(Double.parseDouble(point.getText()));
                        q.setImageAllowed(addDescriptiveQuestion.image);
                        q.setFileAllowed(addDescriptiveQuestion.file);
                        questionsSection.update();

                    } else if (state.equals("true false")) {

                        TrueFalseQuestion q = (TrueFalseQuestion) question;
                        q.setTitle(name.getText());
                        q.setBody(body.getText());
                        q.setDuration(Time.toDate(duration.getText()));
                        q.setPoint(Double.parseDouble(point.getText()));
                        q.setCorrectAnswer(addTrueFalseQuestion.answerKey);
                        questionsSection.update();

                    } else {

                        MultiChoiceQuestion q = (MultiChoiceQuestion) question;
                        q.setTitle(name.getText());
                        q.setBody(body.getText());
                        q.setDuration(Time.toDate(duration.getText()));
                        q.setPoint(Double.parseDouble(point.getText()));
                        q.setChoices(addMultiChoiceQuestion.getChoices());
                        q.setCorrectAnswer(addMultiChoiceQuestion.getCorrectAnswer());
                        questionsSection.update();

                    }

                }

                return;
            }

            if (checkStatus()) {
                if (state.equals("descriptive")) {

                    DescriptiveQuestion q = new DescriptiveQuestion();
                    q.setTitle(name.getText());
                    q.setBody(body.getText());
                    q.setDuration(Time.toDate(duration.getText()));
                    q.setPoint(Double.parseDouble(point.getText()));
                    q.setImageAllowed(addDescriptiveQuestion.image);
                    q.setFileAllowed(addDescriptiveQuestion.file);
                    questionsSection.submit(q);

                } else if (state.equals("true false")) {

                    TrueFalseQuestion q = new TrueFalseQuestion();
                    q.setTitle(name.getText());
                    q.setBody(body.getText());
                    q.setDuration(Time.toDate(duration.getText()));
                    q.setPoint(Double.parseDouble(point.getText()));
                    q.setCorrectAnswer(addTrueFalseQuestion.answerKey);
                    questionsSection.submit(q);

                } else {

                    MultiChoiceQuestion q = new MultiChoiceQuestion();
                    q.setTitle(name.getText());
                    q.setBody(body.getText());
                    q.setDuration(Time.toDate(duration.getText()));
                    q.setPoint(Double.parseDouble(point.getText()));
                    q.setChoices(addMultiChoiceQuestion.getChoices());
                    q.setCorrectAnswer(addMultiChoiceQuestion.getCorrectAnswer());

                    questionsSection.submit(q);

                }

            }

        });

    }

    private boolean checkStatus() {

        if (name.getText().equals("")) return false;
        if (body.getText().equals("")) return false;
        if (point.getText().equals("")) return false;

        if (state.equals("multi choice")) {
             if (addMultiChoiceQuestion.options.size() == 0) return false;
        }

        return true;
    }

    public void setExamInfo(ExamInfo examInfo) {
        this.examInfo = examInfo;
    }

    public void setQuestionsSection(QuestionsSection questionsSection) {
        this.questionsSection = questionsSection;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public void setQuestion(Question question, boolean outdated) {
        this.question = question;
        this.outdated = outdated;

        if (question == null) {
            renderDescriptive();
            return;
        }

        descriptive.setOnMouseClicked(null);
        trueFalse.setOnMouseClicked(null);
        multiChoice.setOnMouseClicked(null);

        if (outdated) {
            name.setDisable(true);
            name.setText(question.getTitle());
            body.setDisable(true);
            body.setText(question.getBody());
            duration.setDisable(true);
            if (question.getDuration() != null) duration.setText(question.getDuration().toString());
            else duration.setText("none");
            point.setDisable(true);
            point.setText(String.valueOf(question.getPoint()));
            apply.setDisable(true);
        } else {
            name.setText(question.getTitle());
            body.setText(question.getBody());
            if (question.getDuration() != null) duration.setText(question.getDuration().toString());
            point.setText(String.valueOf(question.getPoint()));
        }

        if (question instanceof DescriptiveQuestion) renderDescriptive();
        if (question instanceof TrueFalseQuestion) renderTrueFalse();
        if (question instanceof MultiChoiceQuestion) renderMultiChoice();

    }

    private void setType() {

        switch (state) {
            case "descriptive" -> {
                state = "descriptive";
                descriptive.setTextFill(Color.valueOf("#1BA098"));
                trueFalse.setTextFill(Color.valueOf("#707070"));
                multiChoice.setTextFill(Color.valueOf("#707070"));
            }

            case "true false" -> {
                state = "true false";
                descriptive.setTextFill(Color.valueOf("#707070"));
                trueFalse.setTextFill(Color.valueOf("#1BA098"));
                multiChoice.setTextFill(Color.valueOf("#707070"));
            }

            case "multi choice" -> {
                state = "multi choice";
                descriptive.setTextFill(Color.valueOf("#707070"));
                trueFalse.setTextFill(Color.valueOf("#707070"));
                multiChoice.setTextFill(Color.valueOf("#1BA098"));
            }
        }

    }

    private void questionsOffCurrentPanel() {

        switch (state) {
            case "descriptive" -> {
                contentLayout.getChildren().removeAll(addDescriptiveQuestionLayout);
            }

            case "true false" -> {
                contentLayout.getChildren().removeAll(addTrueFalseQuestionLayout);
            }

            case "multi choice" -> {
                contentLayout.getChildren().removeAll(addMultiChoiceQuestionLayout);
            }
        }

    }

    public void renderDescriptive() {

        questionsOffCurrentPanel();
        state = "descriptive";
        setType();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("addDescriptiveQuestion.fxml"));
            addDescriptiveQuestionLayout = loader.load();
            addDescriptiveQuestion = loader.getController();
            addDescriptiveQuestion.setQuestion(question);
            contentLayout.getChildren().add(addDescriptiveQuestionLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void renderTrueFalse() {

        questionsOffCurrentPanel();
        state = "true false";
        setType();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("addTrueFalseQuestion.fxml"));
            addTrueFalseQuestionLayout = loader.load();
            addTrueFalseQuestion = loader.getController();
            addTrueFalseQuestion.setQuestion(question);
            contentLayout.getChildren().add(addTrueFalseQuestionLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void renderMultiChoice() {

        questionsOffCurrentPanel();
        state = "multi choice";
        setType();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("addMultiChoiceQuestion.fxml"));
            addMultiChoiceQuestionLayout = loader.load();
            addMultiChoiceQuestion = loader.getController();
            addMultiChoiceQuestion.setQuestion(question);
            contentLayout.getChildren().add(addMultiChoiceQuestionLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
