package components.dashboard.teacher.exams.examInfo.question;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import logic.exam.question.Choice;
import logic.exam.question.MultiChoiceQuestion;
import logic.exam.question.Question;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class AddMultiChoiceQuestion {

    public ImageView add;
    public HashMap<Integer, TextField> options = new HashMap<>();
    public HashMap<Integer, HBox> hs = new HashMap<>();
    public VBox flow;

    private Question question;
    private int counter = 0;
    private Choice corrcet;
    private ImageView star;

    @FXML
    public void initialize() {
        add.setCursor(Cursor.HAND);

        File f = new File("src/assets/images/star.png");
        Image i = new Image(f.toURI().toString());
        star = new ImageView(i);
        star.setFitWidth(25);
        star.setFitHeight(25);

        add.setOnMouseClicked((e) -> {
            counter++;
            HBox h = new HBox();
            h.setAlignment(Pos.CENTER);
            TextField textField = new TextField();
            h.getChildren().add(textField);

            int it = counter;
            h.setOnMouseClicked((ev) -> {
                setCorrect(it);
            });

            flow.getChildren().add(h);
            hs.put(counter, h);
            options.put(counter, textField);

        });
    }

    private void setCorrect(int counter) {

        corrcet = new Choice();
        corrcet.setNumber(counter);
        corrcet.setText(options.get(counter).getText());

        for (int i = 1; i <= hs.size(); i++) {
            hs.get(i).getChildren().removeAll(star);
        }

        hs.get(counter).getChildren().add(star);

    }

    public void setQuestion(Question question) {
        this.question = question;

        if (question != null) {
            flow.getChildren().removeAll(add);
            MultiChoiceQuestion holder = (MultiChoiceQuestion) question;
            for (Choice choice : holder.getChoices()) {

                Label label = new Label();
                label.setText(choice.getText());
                flow.getChildren().add(label);
            }
        }

    }

    public ArrayList<Choice> getChoices() {

        ArrayList<Choice> choices = new ArrayList<>();
        for (int i = 1; i < options.size(); i++) {
            Choice temp = new Choice();
            temp.setNumber(i);
            temp.setText(options.get(i).getText());
            choices.add(temp);
        }

        return choices;

    }

    public Choice getCorrectAnswer() {
        Choice choice = new Choice();
        choice.setNumber(1);
        choice.setText(options.get(1).getText());
        return choice;
    }

}
