package components.dashboard.teacher.exams.examInfo;

import components.dashboard.teacher.TeacherDashboard;
import components.dashboard.teacher.exams.examInfo.participant.AddParticipant;
import components.dashboard.teacher.exams.examInfo.participant.BannedCard;
import components.dashboard.teacher.exams.examInfo.participant.ParticipantsSection;
import components.dashboard.teacher.exams.examInfo.question.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.Date;
import logic.Time;
import logic.exam.Exam;
import logic.exam.question.Question;
import logic.people.Participant;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class ExamInfo {

    public DatePicker date;
    public TextField time;
    public TextField duration;
    public Label reviewable;
    public TextField name;
    public VBox participantsContent;
    public VBox bannedLayout;
    public Button accept;
    public Button cancel;
    public Button result;
    private VBox participantsLayout;
    private VBox addParticipantLayout;
    private ParticipantsSection participantsSectionController;
    private AddParticipant addParticipantController;
    public VBox questionsContent;
    private VBox questionsLayout;
    private VBox addQuestionLayout;
    private QuestionsSection questionsSection;
    private AddQuestion addQuestion;

    private Exam exam;
    private boolean outdated = false;
    private boolean edit = false;
    private boolean shuffled = false;
    private String participantsState = "participants";
    private String questionsState = "questions";
    private boolean reviewabilty = true;
    private TeacherDashboard td;

    public void setExam(Exam exam, boolean outdated) {
        this.exam = exam;

        if (exam != null) {
            edit = true;
            this.outdated = outdated;
            if (outdated) disableEdit();
            name.setText(exam.getTitle());
            if (!exam.isReviewable()) toggleReviewable();
            if (exam.getDuration() != null) duration.setText(exam.getDuration().toString());
            time.setText(exam.getStartTime().toString());
            LocalDate localDate = LocalDate.of(exam.getStartDate().getYear(), exam.getStartDate().getMonth(), exam.getStartDate().getDay());
            date.setValue(localDate);
        } else {
            this.exam = new Exam();
            date.setValue(LocalDate.now());
        }

        renderParticipants();
        renderQuestions();
        renderBanned();

        if (exam == null || !outdated) {
            reviewable.setOnMouseClicked((e) -> {
                toggleReviewable();
            });
        }

        cancel.setOnMouseClicked((e) -> {
            td.setPanel("exams");
        });

        accept.setOnMouseClicked((e) -> {
            submit();
        });

        result.setOnMouseClicked((e) -> {
            td.renderExamResults(exam);
        });

    }

    private void disableEdit() {

        name.setDisable(true);
        reviewable.setOnMouseClicked(null);
        duration.setDisable(true);
        time.setDisable(true);
        date.setDisable(true);

    }

    private void submit() {

        exam.setTitle(name.getText());
        exam.setReviewable(reviewabilty);
        exam.setStartDate(new Date(date.getValue().getYear(), date.getValue().getMonthValue(), date.getValue().getDayOfMonth()));
        exam.setStartTime(Time.toDate(time.getText()));
        exam.setDuration(Time.toDate(duration.getText()));

        if (edit) {
            td.editExam(exam);
        } else {
            td.saveExam(exam);
        }

    }

    private void toggleReviewable() {

        if (reviewabilty) {
            reviewabilty = false;
            reviewable.setTextFill(Color.valueOf("#DEB992"));
            reviewable.setStyle("-fx-background-color: #EAEDF4;" + "-fx-border-width: 2;" + "-fx-border-color: #DEB992");
        } else {
            reviewabilty = true;
            reviewable.setTextFill(Color.valueOf("#fff"));
            reviewable.setStyle("-fx-background-color: #DEB992;" + "-fx-border-width: 2;" + "-fx-border-color: #EAEDF4");
        }

    }

    private void participantsOffCurrentPanel() {

        switch (participantsState) {
            case "participants" -> {
                participantsContent.getChildren().removeAll(participantsLayout);
            }

            case "add participant" -> {
                participantsContent.getChildren().removeAll(addParticipantLayout);
            }
        }

    }

    public void renderParticipants() {

        participantsOffCurrentPanel();
        participantsState = "participants";

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("participant/participantsSection.fxml"));
            participantsLayout = loader.load();
            participantsSectionController = loader.getController();
            participantsSectionController.setExamInfo(this, outdated);
            participantsSectionController.setExam(exam);
            participantsSectionController.setShuffled(shuffled);
            participantsSectionController.setParticipants(exam.getParticipants());
            participantsContent.getChildren().add(participantsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void renderAddParticipant() {

        participantsOffCurrentPanel();
        participantsState = "add participant";

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("participant/addParticipant.fxml"));
            addParticipantLayout = loader.load();
            addParticipantController = loader.getController();
            addParticipantController.setExam(exam);
            addParticipantController.setExamInfo(this);
            participantsContent.getChildren().add(addParticipantLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void questionsOffCurrentPanel() {

        switch (questionsState) {
            case "questions" -> {
                questionsContent.getChildren().removeAll(questionsLayout);
            }

            case "add question" -> {
                questionsContent.getChildren().removeAll(addQuestionLayout);
            }

        }

    }

    public void renderQuestions() {

        questionsOffCurrentPanel();
        questionsState = "questions";

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("question/questionsSection.fxml"));
            questionsLayout = loader.load();
            questionsSection = loader.getController();
            questionsSection.setExamInfo(this, outdated);
            questionsSection.setQuestions(exam.getQuestions());
            questionsSection.setExam(exam);
            questionsContent.getChildren().add(questionsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void renderAddQuestion(Question question) {

        questionsOffCurrentPanel();
        questionsState = "add question";

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("question/addQuestion.fxml"));
            addQuestionLayout = loader.load();
            addQuestion = loader.getController();
            addQuestion.setExamInfo(this);
            addQuestion.setQuestionsSection(questionsSection);
            addQuestion.setQuestion(question, outdated);
            addQuestion.setExam(exam);
            questionsContent.getChildren().add(addQuestionLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void handleDragOver(DragEvent event) {

        if (event.getGestureSource() != bannedLayout && event.getDragboard().hasString()) {
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }

        event.consume();

    }

    public void handleDragDrop(DragEvent event) {

        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            banStudent(db.getString());
            success = true;
        }
        event.setDropCompleted(success);

        event.consume();

    }

    private void banStudent(String id) {

        for (Participant participant : exam.getParticipants()) {
            if (participant.getStudentId().equals(id)) {
                participant.setBanned(true);
                break;
            }
        }

        renderParticipants();
        renderBanned();

    }

    private void renderBanned() {

        bannedLayout.getChildren().clear();
        Label l = new Label("دانشجویان محروم");
        l.setStyle("-fx-font-family: 'Vazir Bold';" + "-fx-font-size: 24px");
        l.setTextFill(Color.valueOf("#1BA098"));
        bannedLayout.getChildren().add(l);

        for (Participant participant : exam.getParticipants()) {
            if (participant.isBanned()) {

                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("participant/bannedCard.fxml"));
                    HBox holderLayout = loader.load();
                    BannedCard holderController = loader.getController();
                    holderController.setExamInfo(this);
                    holderController.setInfo(participant);
                    bannedLayout.getChildren().add(holderLayout);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }

    }

    public void setTd(TeacherDashboard td) {
        this.td = td;
    }

    public void shuffle() {
        shuffled = true;
        for (Participant p : exam.getParticipants()) {
            Collections.shuffle(p.getQuestionQueue());
        }

        renderParticipants();

    }

    public void unShuffle() {
        shuffled = false;
        for (Participant p : exam.getParticipants()) {
            Collections.sort(p.getQuestionQueue());
        }

        renderParticipants();

    }

    public void unbanDrop(DragEvent event) {

        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            unbanStudent(db.getString());
            success = true;
        }
        event.setDropCompleted(success);

        event.consume();

    }

    private void unbanStudent(String id) {

        for (Participant participant : exam.getParticipants()) {
            if (participant.getStudentId().equals(id)) {
                participant.setBanned(false);
                break;
            }
        }

        renderParticipants();
        renderBanned();

    }

    public void unbanOver(DragEvent event) {

        if (event.getGestureSource() != participantsLayout && event.getDragboard().hasString()) {
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }

        event.consume();

    }
}
