package components.dashboard.teacher.exams.examInfo.participant;

import components.dashboard.teacher.exams.examInfo.ExamInfo;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import logic.exam.Exam;
import logic.exam.question.Question;
import logic.people.Participant;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ParticipantsSection {

    public ImageView add;
    public ImageView shuffle;
    public VBox cardsLayout;
    public VBox veryLayout;
    public HBox omgLayout;
    private ArrayList<Participant> participants;
    private final HashMap<Integer, VBox> layouts = new HashMap<>();
    private final HashMap<Integer, ParticipantCard> controllers = new HashMap<>();
    private ExamInfo examInfo;
    private boolean outdated;
    public boolean shuffled = false;
    private Exam exam;

    private Image shuffle_on;
    private Image shuffle_off;

    @FXML
    public void initialize() {

        File file = new File("src/assets/images/shuffle_on.png");
        shuffle_on = new Image(file.toURI().toString());

        file = new File("src/assets/images/shuffle_off.png");
        shuffle_off = new Image(file.toURI().toString());

        shuffle.setCursor(Cursor.HAND);
        add.setCursor(Cursor.HAND);

    }

    public void setParticipants(ArrayList<Participant> participants) {
        this.participants = participants;
        renderParticipants();

        shuffle.setOnMouseClicked((e) -> {

            if (shuffled) {
                examInfo.unShuffle();
            } else {
                examInfo.shuffle();
            }

        });
    }

    public void setExamInfo(ExamInfo examInfo, boolean outdated) {
        this.examInfo = examInfo;
        add.setOnMouseClicked((e) -> {
            examInfo.renderAddParticipant();
        });
        this.outdated = outdated;
        if (outdated) {
            omgLayout.getChildren().removeAll(add);
        }
    }

    public void renderParticipants() {

        int i = 0;
        for (Participant participant : participants) {

            if (!participant.isBanned()) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("participantCard.fxml"));
                    VBox holderLayout = loader.load();
                    ParticipantCard holderController = loader.getController();
                    holderController.setExamInfo(examInfo, outdated);
                    holderController.setInfo(participant);
                    controllers.put(i, holderController);
                    cardsLayout.getChildren().add(holderLayout);
                    layouts.put(i, holderLayout);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                i++;
            }

        }

    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public void setShuffled(boolean shuffled) {
        this.shuffled = shuffled;

        if (shuffled) {
            shuffle.setImage(shuffle_on);
        } else {
            shuffle.setImage(shuffle_off);
        }
    }
}
