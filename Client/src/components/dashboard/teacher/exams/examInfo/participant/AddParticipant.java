package components.dashboard.teacher.exams.examInfo.participant;

import components.dashboard.teacher.exams.examInfo.ExamInfo;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import logic.exam.Exam;
import logic.exam.question.Question;
import logic.people.Participant;
import utils.ExcelHandler;

import java.io.File;
import java.util.ArrayList;

public class AddParticipant {

    public TextField firstname;
    public TextField lastname;
    public TextField studentId;
    public Button excelupload;
    public Button cancel;
    public Button addButton;

    private Exam exam;
    private File excel;
    private ExcelHandler excelHandler = new ExcelHandler();
    private FileChooser fileChooser = new FileChooser();
    private ExamInfo examInfo;

    @FXML
    public void initialize() {

        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel Files", "*.xlsx")
        );

        cancel.setOnMouseClicked((e) -> {
            examInfo.renderParticipants();
        });

    }

    public void setExamInfo(ExamInfo examInfo) {
        this.examInfo = examInfo;
    }

    public void setExam(Exam exam){

        this.exam = exam;
        addButton.setOnMouseClicked((e) -> {
            boolean ok = true;
            if (firstname.getText().equals("")) {
                ok = false;
            }

            if (lastname.getText().equals("")) {
                ok = false;
            }

            if (studentId.getText().equals("")) {
                ok = false;
            }

            if (ok) {
                Participant participant = new Participant();
                participant.setUsername(studentId.getText());
                participant.setFirstName(firstname.getText());
                participant.setLastName(lastname.getText());
                participant.setPassword(studentId.getText());
                participant.setStudentId(studentId.getText());

                for (Question q : exam.getQuestions()) {
                    if (!participant.getQuestionQueue().contains(q.getNumber())) {
                        participant.getQuestionQueue().add(q.getNumber());
                    }
                }

                exam.setParticipant(participant);
                examInfo.renderParticipants();
            }

        });

        excelupload.setOnMouseClicked((e) -> {
            excel = fileChooser.showOpenDialog(new Stage());
            excelupload.setText("پردازش...");

            ArrayList<Participant> participants = excelHandler.setParticipants(excel);
            for (Participant p : participants) {
                p.setPassword(p.getStudentId());
                p.setUsername(p.getStudentId());

                for (Question q : exam.getQuestions()) {
                    if (!p.getQuestionQueue().contains(q.getNumber())) {
                        p.getQuestionQueue().add(q.getNumber());
                        System.out.println("&&&&&");
                    }
                }
                exam.getParticipants().add(p);
            }

            examInfo.renderParticipants();
        });

    }

}
