package components.dashboard.teacher.exams.examInfo.participant;

import components.dashboard.teacher.exams.examInfo.ExamInfo;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.people.Participant;

import java.util.ArrayList;

public class ParticipantCard {

    public Label studentId;
    public Label name;
    public HBox queue;
    public VBox participantCard;
    private Participant participant;

    private ExamInfo examInfo;
    private boolean outdated;

    public void setInfo(Participant participant) {

        this.participant = participant;
        name.setText(participant.getFirstName() + " " + participant.getLastName());
        studentId.setText(participant.getStudentId());
        renderQueue(participant.getQuestionQueue());

    }

    public void setExamInfo(ExamInfo examInfo, boolean outdated) {
        this.examInfo = examInfo;

        if (outdated) {
            participantCard.setOnDragDetected(null);
        }
    }

    private void renderQueue(ArrayList<Integer> questions) {

        for (int num : questions) {

            Label label = new Label();
            label.setText(String.valueOf(num));
            label.setPrefWidth(24);
            label.setPrefHeight(24);
            label.setAlignment(Pos.CENTER);
            label.setTextFill(Color.valueOf("#1BA098"));
            label.setStyle("-fx-border-width: 2;" + "-fx-border-color: #1BA098;" + "-fx-font-size: 16px;" + "-fx-font-family: 'Vazir Bold'");
            queue.getChildren().add(label);

        }

    }

    public void handleDrag(MouseEvent mouseEvent) {
        Dragboard db = participantCard.startDragAndDrop(TransferMode.ANY);

        ClipboardContent content = new ClipboardContent();
        content.putString(participant.getStudentId());
        db.setContent(content);

        mouseEvent.consume();
    }

}
