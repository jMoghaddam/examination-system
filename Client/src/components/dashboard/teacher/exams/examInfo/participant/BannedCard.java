package components.dashboard.teacher.exams.examInfo.participant;

import components.dashboard.teacher.exams.examInfo.ExamInfo;
import javafx.scene.control.Label;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import logic.people.Participant;

public class BannedCard {

    public Label studentId;
    public Label name;
    public HBox bannedCard;

    private Participant participant;
    private ExamInfo examInfo;

    public void setInfo(Participant participant) {

        this.participant = participant;
        name.setText(participant.getFirstName() + " " + participant.getLastName());
        studentId.setText(participant.getStudentId());

    }

    public void setExamInfo(ExamInfo examInfo) {
        this.examInfo = examInfo;
    }

    public void handleDrag(MouseEvent mouseEvent) {
        Dragboard db = bannedCard.startDragAndDrop(TransferMode.ANY);

        ClipboardContent content = new ClipboardContent();
        content.putString(participant.getStudentId());
        db.setContent(content);

        mouseEvent.consume();
    }

}
