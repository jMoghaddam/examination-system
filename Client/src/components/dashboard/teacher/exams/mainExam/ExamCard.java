package components.dashboard.teacher.exams.mainExam;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.Date;
import logic.Time;
import logic.exam.Exam;
import logic.exam.question.Question;
import logic.people.Participant;
import logic.people.Teacher;
import model.UserSetInfoHandler;
import org.apache.poi.ss.formula.functions.T;
import utils.ControlledScreen;
import utils.ScreenController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

public class ExamCard implements ControlledScreen {

    // UI Elements
    public Label title;
    public Label date;
    public Label time;
    public ProgressBar progress;
    public Button actionButton;
    public VBox examCardLayout;

    private MainExamSection mainExamSection; // panel transition
    private Exam exam; // exam
    private String condition = "pending"; // Current State
    private ScreenController sc; // screen transition
    private Teacher user; // user
    private UserSetInfoHandler userSetInfoHandler = new UserSetInfoHandler();

    public void setExamInfo(Exam exam) {
        this.exam = exam;
        title.setText(exam.getTitle());
        date.setText(exam.getStartDate().toString());
        time.setText(exam.getStartTime().toString());

        double p = (double) exam.getResult().getGraded() / (exam.getParticipants().size() * exam.getQuestions().size());
        progress.setProgress(p);

        LocalDateTime ldt = LocalDateTime.now();
        Date d = new Date(ldt.getYear(), ldt.getMonthValue(), ldt.getDayOfMonth());
        if (Date.isBigger(exam.getStartDate(), d) > 0) {
            condition = "pending";
        } else if (Date.isBigger(exam.getStartDate(), d) < 0) {
            condition = "grading";
        } else {

            Time t = new Time();
            t.setHour(ldt.getHour());
            t.setMin(ldt.getMinute());

            Time t1 = new Time();
            if (exam.getDuration() != null) {
                t1.setHour(exam.getStartTime().getHour() + exam.getDuration().getHour());
                t1.setMin(exam.getStartTime().getMin() + exam.getDuration().getMin());
            } else {
                int h = 0;
                int m = 0;
                for (Question q : exam.getQuestions()) {
                    h += q.getDuration().getHour();
                    m += q.getDuration().getMin();
                }
                while (m > 60) {
                    h++;
                    m -= 60;
                }
                t1.setHour(exam.getStartTime().getHour() + h);
                t1.setMin(exam.getStartTime().getMin() + m);
            }

            if (Time.isBigger(t1, t) > 0) {
                condition = "pending";
            } else {
                condition = "grading";
            }

        }

        if (exam.getResult().getGraded() == exam.getParticipants().size() * exam.getQuestions().size()) {
            condition = "over";
        }

        if (condition.equals("pending")) {
            actionButton.setStyle("-fx-background-color: #707070");
            actionButton.setText("در انتظار");
            progress.setVisible(false);
        }

        if (condition.equals("over")) {
            actionButton.setStyle("-fx-background-color: #051622");
            actionButton.setText("تمام شده");
            progress.setVisible(true);
            ranking();
        }

        if (condition.equals("grading")) {
            actionButton.setStyle("-fx-background-color: #1BA098");
            actionButton.setText("تصحیح");
            actionButton.setOnMouseClicked((e) -> {
                sc.setGradingScreen(exam, user);
            });
        }

    }

    private void ranking() {

        ArrayList<Participant> ranked = exam.getParticipants();
        Collections.sort(ranked);

        int i = ranked.size();
        for (Participant p : ranked) {
            p.setRank(i);
            i--;
        }
        userSetInfoHandler.saveRanks(exam);

    }

    public void setMainExamSection(MainExamSection mainExamSection) {
        this.mainExamSection = mainExamSection;

        examCardLayout.setOnMouseClicked((e) -> {
            mainExamSection.renderExamInfo(exam, !  condition.equals("pending"));
        });

    }

    @Override
    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }

    public void setUser(Teacher user) {
        this.user = user;
    }
}
