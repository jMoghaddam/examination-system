package components.dashboard.teacher.exams.mainExam;

import components.dashboard.teacher.TeacherDashboard;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.Date;
import logic.Time;
import logic.exam.Exam;
import logic.exam.question.Question;
import logic.people.Participant;
import model.UserGetInfoHandler;
import utils.ControlledScreen;
import utils.ScreenController;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

public class MainExamSection implements ControlledScreen {

    // UI Elements
    public Label all;
    public Label ungraded;
    public HBox contentLayout;
    public ImageView add = new ImageView();

    // Sub Components
    private final HashMap<Integer, VBox> layouts = new HashMap<>();
    private final HashMap<Integer, ExamCard> controllers = new HashMap<Integer, ExamCard>();
    public FlowPane flow;

    private UserGetInfoHandler userGetInfoHandler = new UserGetInfoHandler(); // database
    private boolean ungradedFilter = false; // filter
    private ArrayList<Exam> exams; // exams list
    private TeacherDashboard td; // panel transition
    private ScreenController sc; // screen transition

    public void setTd(TeacherDashboard td) {
        this.td = td;
    }

    public void setUserInfo(ArrayList<Exam> exams) {
        this.exams = exams;
        renderExamCards();

        File f = new File("src/assets/images/add.png");
        Image i = new Image(f.toURI().toString());
        add.setImage(i);
        add.setFitWidth(100);
        add.setFitHeight(100);

        add.setOnMouseClicked((e) -> {
            td.renderNewExam();
        });

        all.setOnMouseClicked((e) -> {
            all.setTextFill(Color.valueOf("#1BA098"));
            ungraded.setTextFill(Color.valueOf("#707070"));
            ungradedFilter = false;
            renderExamCards();
        });

        ungraded.setOnMouseClicked((e) -> {
            ungraded.setTextFill(Color.valueOf("#1BA098"));
            all.setTextFill(Color.valueOf("#707070"));
            ungradedFilter = true;
            renderExamCards();
        });
    }

    private boolean checkRender(Exam exam) {

        if (!ungradedFilter) return true;
        else if (exam.getResult().getGraded() == exam.getParticipants().size() * exam.getQuestions().size()) return false;
        else {
            LocalDateTime ldt = LocalDateTime.now();
            Date d = new Date(ldt.getYear(), ldt.getMonthValue(), ldt.getDayOfMonth());
            if (Date.isBigger(exam.getStartDate(), d) > 0) {
                return false;
            } else if (Date.isBigger(exam.getStartDate(), d) < 0) {
                return true;
            } else {

                Time t = new Time();
                t.setHour(ldt.getHour());
                t.setMin(ldt.getMinute());

                Time t1 = new Time();
                if (exam.getDuration() != null) {
                    t1.setHour(exam.getStartTime().getHour() + exam.getDuration().getHour());
                    t1.setMin(exam.getStartTime().getMin() + exam.getDuration().getMin());
                } else {
                    int h = 0;
                    int m = 0;
                    for (Question q : exam.getQuestions()) {
                        h += q.getDuration().getHour();
                        m += q.getDuration().getMin();
                    }
                    while (m > 60) {
                        h++;
                        m -= 60;
                    }
                    t1.setHour(exam.getStartTime().getHour() + h);
                    t1.setMin(exam.getStartTime().getMin() + m);
                }

                return Time.isBigger(t1, t) <= 0;

            }
        }

    }

    private void renderExamCards() {

        flow.getChildren().clear();
        flow.getChildren().add(add);

        int i = 0;
        for (Exam exam : exams) {

            if (checkRender(exam)) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("examCard.fxml"));
                    VBox holderLayout = loader.load();
                    ExamCard holderController = loader.getController();
                    holderController.setScreen(sc);
                    holderController.setUser(td.user);
                    holderController.setExamInfo(exam);
                    holderController.setMainExamSection(this);
                    controllers.put(i, holderController);
                    int btnIndex = flow.getChildren().indexOf(add);
                    flow.getChildren().add(btnIndex, holderLayout);
                    layouts.put(i, holderLayout);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                i++;
            }

        }

    }

    public void renderExamInfo(Exam exam, boolean outdated) {
        td.renderExam(exam, outdated);
    }

    @Override
    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }
}
