package components.dashboard.teacher.exams;

import components.dashboard.teacher.TeacherDashboard;
import components.dashboard.teacher.exams.mainExam.MainExamSection;
import components.dashboard.teacher.exams.results.ResultsSection;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.exam.Exam;
import logic.people.Teacher;
import model.UserGetInfoHandler;
import utils.ControlledScreen;
import utils.ScreenController;

import java.io.IOException;
import java.util.ArrayList;

public class ExamsSection implements ControlledScreen {

    // UI Elements
    public VBox contentLayout;
    public Label examsOption;
    public Label resultsOption;

    // Sub Components
    private VBox mainExamsLayout;
    private HBox resultsLayout;
    private MainExamSection mainExamSectionController;
    private ResultsSection resultsSectionController;

    private UserGetInfoHandler userGetInfoHandler = new UserGetInfoHandler(); // database
    private ArrayList<Exam> exams; // exams list
    private TeacherDashboard td; // panel transition
    private String section = "main"; // Current State
    private ScreenController sc; // screen transition
    private Teacher user;

    @FXML
    public void initialize() {

        examsOption.setOnMouseClicked((e) -> setSection("main"));
        resultsOption.setOnMouseClicked((e) -> setSection("results"));

    }

    private void setSection(String section) {

        offCurrentSection();
        this.section = section;

        switch (section) {
            case "main" -> {
                examsOption.setTextFill(Color.valueOf("#1BA098"));
                renderExamsSection();
            }

            case "results" -> {
                resultsOption.setTextFill(Color.valueOf("#1BA098"));
                renderResultsSection();
            }
        }

    }

    public void setUserInfo(ArrayList<Exam> exams) {
        this.exams = exams;
        renderExamsSection();
    }

    public void setTd(TeacherDashboard td) {
        this.td = td;
    }

    private void offCurrentSection() {

        switch (section) {
            case "main" -> {
                examsOption.setTextFill(Color.valueOf("#707070"));
                contentLayout.getChildren().removeAll(mainExamsLayout);
            }

            case "results" -> {
                resultsOption.setTextFill(Color.valueOf("#707070"));
                contentLayout.getChildren().removeAll(resultsLayout);
            }
        }

    }

    private void renderExamsSection() {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("mainExam/mainExamSection.fxml"));
            mainExamsLayout = loader.load();
            mainExamSectionController = loader.getController();
            mainExamSectionController.setScreen(sc);
            mainExamSectionController.setTd(td);
            mainExamSectionController.setUserInfo(exams);
            contentLayout.getChildren().add(mainExamsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void renderResultsSection() {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("results/resultsSection.fxml"));
            resultsLayout = loader.load();
            resultsSectionController = loader.getController();
            resultsSectionController.setExams(exams);
            resultsSectionController.setUser(user);
            contentLayout.getChildren().add(resultsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }

    public void setUser(Teacher user) {
        this.user = user;
    }
}
