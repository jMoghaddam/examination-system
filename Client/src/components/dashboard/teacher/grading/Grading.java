package components.dashboard.teacher.grading;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.exam.Exam;
import logic.exam.answer.Answer;
import logic.exam.answer.DescriptiveAnswer;
import logic.exam.answer.MultiChoiceAnswer;
import logic.exam.answer.TrueFalseAnswer;
import logic.exam.question.DescriptiveQuestion;
import logic.exam.question.MultiChoiceQuestion;
import logic.exam.question.Question;
import logic.exam.question.TrueFalseQuestion;
import logic.people.Participant;
import logic.people.Teacher;
import model.UserSetInfoHandler;
import utils.ScreenController;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Grading {

    public VBox queue;
    public VBox contentLayout;
    public VBox answerLayout;
    public TextField gradeBox;
    public Label info;
    public Label body;
    private Exam exam;
    private ScreenController sc;
    private Answer currentAnswer;
    private Participant currentPar;
    private UserSetInfoHandler userSetInfoHandler = new UserSetInfoHandler();
    private Teacher user;

    public void setExam(Exam exam) {
        this.exam = exam;

        for (Participant participant : exam.getParticipants()) {

            HBox h = new HBox();
            h.setFillHeight(false);
            h.setSpacing(10.0);
            Label sid = new Label(participant.getStudentId());
            sid.setStyle("-fx-font-size: 24px;" + "-fx-font-family: 'Vazir Bold'");
            sid.setTextFill(Color.valueOf("#1BA098"));
            h.getChildren().add(sid);

            for (int i : participant.getQuestionQueue()) {
                Label q = new Label(String.valueOf(i));
                q.setStyle("-fx-font-size: 24px;" + "-fx-font-family: 'Vazir';");
                q.setCursor(Cursor.HAND);
                q.setPadding(new Insets(5, 5, 5, 5));
                h.getChildren().add(q);

                q.setOnMouseClicked((e) -> {
                    currentPar = participant;
                    renderQuestion(participant, i);
                });

            }

            queue.getChildren().add(h);

        }

        currentPar = exam.getParticipants().get(0);
        renderQuestion(exam.getParticipants().get(0), 1);

    }

    private void renderQuestion(Participant participant, int i) {

        answerLayout.getChildren().clear();
        Question q = exam.getQuestions().get(i - 1);

        Answer test = null;
        for (Answer a : participant.getAnswers()) {
            if (a.getNumber() == q.getNumber()) {
                test = a;
                break;
            }
        }
        currentAnswer = test;

        if (q instanceof DescriptiveQuestion) {
            DescriptiveAnswer a = (DescriptiveAnswer) test;
            if (a.getText() == null) {
                info.setText("بی پاسخ");
                gradeBox.setText("0.0");
                return;
            }
        }

        if (q instanceof TrueFalseQuestion) {
            TrueFalseAnswer a = (TrueFalseAnswer) test;
            if (!a.isHasAnswered()) {
                info.setText("بی پاسخ");
                gradeBox.setText("0.0");
                return;
            }
        }

        if (q instanceof MultiChoiceQuestion) {
            MultiChoiceAnswer a = (MultiChoiceAnswer) test;
            if (a.getAnswer() == null) {
                info.setText("بی پاسخ");
                gradeBox.setText("0.0");
                return;
            }
        }

        info.setText(q.getTitle());
        body.setText(q.getBody());

        if (q instanceof DescriptiveQuestion) {
            DescriptiveAnswer answer = null;

            for (Answer a : participant.getAnswers()) {
                if (a.getNumber() == q.getNumber()) {
                    answer = (DescriptiveAnswer) a;
                    break;
                }
            }

            currentAnswer = answer;

            gradeBox.setText(String.valueOf(answer.getGrade()));

            Label text = new Label(answer.getText());
            answerLayout.getChildren().add(text);

            if (answer.getImage() != null) {
                Label img = new Label("دریافت عکس");
                img.setStyle("-fx-font-size: 24px");
                img.setTextFill(Color.valueOf("#707070"));
                answerLayout.getChildren().add(img);
                DescriptiveAnswer a = answer;
                img.setOnMouseClicked((e) -> {
                    try {
                        Desktop.getDesktop().open(a.getImage());
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                });
            }

            if (answer.getFile() != null) {
                Label file = new Label("دریافت فایل");
                file.setStyle("-fx-font-size: 24px");
                file.setTextFill(Color.valueOf("#707070"));
                answerLayout.getChildren().add(file);
                DescriptiveAnswer a = answer;
                file.setOnMouseClicked((e) -> {
                    try {
                        Desktop.getDesktop().open(a.getFile());
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                });
            }

        }

        if (q instanceof TrueFalseQuestion) {
            TrueFalseAnswer answer = null;

            for (Answer a : participant.getAnswers()) {
                if (a.getNumber() == q.getNumber()) {
                    answer = (TrueFalseAnswer) a;
                    break;
                }
            }
            currentAnswer = answer;

            gradeBox.setText(String.valueOf(answer.getGrade()));

            Label text = new Label("answer: " + answer.isAnswer());
            answerLayout.getChildren().add(text);
        }

        if (q instanceof MultiChoiceQuestion) {
            MultiChoiceAnswer answer = null;

            for (Answer a : participant.getAnswers()) {
                if (a.getNumber() == q.getNumber()) {
                    answer = (MultiChoiceAnswer) a;
                    break;
                }
            }
            currentAnswer = answer;

            gradeBox.setText(String.valueOf(answer.getGrade()));

            Label text = new Label("answer: " + answer.getAnswer().getNumber() + ") " + answer.getAnswer().getText());
            answerLayout.getChildren().add(text);
        }

    }

    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }

    public void save(ActionEvent actionEvent) {

        exam.getResult().gradedInc();
        currentAnswer.setGrade(Double.parseDouble(gradeBox.getText()));
        userSetInfoHandler.saveGraded(exam, currentAnswer, currentPar);

    }

    public void finish(ActionEvent actionEvent) {

        double total = 0.0;
        int counter = 0;

        for (Participant p : exam.getParticipants()) {
            counter++;
            double grade = 0.0;
            for (Answer a : p.getAnswers()) {
                if (!Double.isNaN(a.getGrade()) && grade != exam.getParticipants().size() * exam.getQuestions().size()) grade += a.getGrade();
            }
            total += grade;
            p.setGrade(grade);
            userSetInfoHandler.saveParticipantGrade(exam, p);
        }

        if (exam.getResult().getGraded() == exam.getParticipants().size() * exam.getQuestions().size()) {
            ranking();
        }

        exam.getResult().setAvg(total / counter);
        userSetInfoHandler.saveAvg(exam);
        sc.setTeacherDashboardScreen(user);

    }

    private void ranking() {

        ArrayList<Participant> ranked = exam.getParticipants();
        Collections.sort(ranked);

        int i = ranked.size();
        for (Participant p : ranked) {
            p.setRank(i);
            i--;
        }
        userSetInfoHandler.saveRanks(exam);

    }

    public void setUser(Teacher user) {
        this.user = user;
    }
}
