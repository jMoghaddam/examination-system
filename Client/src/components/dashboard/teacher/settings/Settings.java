package components.dashboard.teacher.settings;

import components.dashboard.student.StudentDashboard;
import components.dashboard.teacher.TeacherDashboard;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import logic.people.Person;
import logic.people.Teacher;
import model.UserSetInfoHandler;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class Settings {

    public Label name;
    public ImageView editName;
    public ImageView avatar;
    public HBox nameBox;
    public TextArea fnameField;
    public TextArea lnameField;
    public TextArea bioField;
    public Label bio;
    public ImageView editBio;
    public HBox bioBox;
    public Label path;
    public Button save;
    private Person user;
    private boolean editNameMode = false;
    private boolean editBioMode = false;
    private FileChooser fileChooser = new FileChooser();
    private DirectoryChooser pathChooser = new DirectoryChooser();
    private UserSetInfoHandler userSetInfoHandler = new UserSetInfoHandler();
    private StudentDashboard sd;
    private TeacherDashboard td;
    private File newPath;

    public void setUser(Person user) {
        this.user = user;
        path.setText(user.getSaveDirectoryPath());
        name.setText(user.getFirstName() + " " + user.getLastName());
        bio.setText(user.getBio());

        fnameField.setText(user.getFirstName());
        lnameField.setText(user.getLastName());
        bioField.setText(user.getBio());

        nameBox.getChildren().removeAll(fnameField);
        nameBox.getChildren().removeAll(lnameField);
        editName.setOnMouseClicked((e) -> {
            toggleEditName();
        });

        pathChooser.setInitialDirectory(new File(user.getSaveDirectoryPath()));
        path.setOnMouseClicked((e) -> {
            newPath = pathChooser.showDialog(new Stage());
            path.setText(user.getSaveDirectoryPath());
        });

        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.jpeg", "*.png")
        );
        avatar.setOnMouseClicked((e) -> {
            File newAvatar = fileChooser.showOpenDialog(new Stage());
            user.setAvatar(newAvatar);
            Image iv = new Image(newAvatar.toURI().toString());
            avatar.setImage(iv);
        });

        bioBox.getChildren().removeAll(bioField);
        editBio.setOnMouseClicked((e) -> {
            toggleEditBio();
        });

        Image i = new Image(user.getAvatar().toURI().toString());
        avatar.setImage(i);
    }

    private void toggleEditBio() {
        editBioMode = !editBioMode;
        if (editBioMode) {
            bioBox.getChildren().removeAll(bio);
            bioBox.getChildren().add(bioField);
            bioField.setText(user.getBio());
        } else {
            bioBox.getChildren().removeAll(bioField);
            bioBox.getChildren().add(bio);
        }
    }

    private void toggleEditName() {
        editNameMode = !editNameMode;
        if (editNameMode) {
            nameBox.getChildren().removeAll(name);
            nameBox.getChildren().add(fnameField);
            fnameField.setText(user.getFirstName());
            nameBox.getChildren().add(lnameField);
            lnameField.setText(user.getLastName());
        } else {
            nameBox.getChildren().removeAll(fnameField);
            nameBox.getChildren().removeAll(lnameField);
            nameBox.getChildren().add(name);
        }
    }

    public void save(ActionEvent actionEvent) {
        user.setFirstName(fnameField.getText());
        user.setLastName(lnameField.getText());
        user.setBio(bioField.getText());

        if (newPath != null) {
            try {
                FileUtils.copyDirectory(new File(user.getSaveDirectoryPath()), newPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            user.setSaveDirectoryPath(newPath.getPath());
        }

        userSetInfoHandler.updateUser(user);
        if (sd != null) sd.update();
        else td.update();
    }

    public void setFather(StudentDashboard studentDashboard) {
        this.sd = studentDashboard;
    }

    public void setFather(TeacherDashboard teacherDashboard) {
        this.td = teacherDashboard;
    }
}
