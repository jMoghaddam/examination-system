package components.dashboard.teacher;

import chat.ChatClient;
import chat.ExamChangeListener;
import components.dashboard.teacher.chats.Chats;
import components.dashboard.teacher.exams.ExamsSection;
import components.dashboard.teacher.exams.examInfo.ExamInfo;
import components.dashboard.teacher.exams.results.ExamResults;
import components.dashboard.teacher.settings.Settings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.exam.Exam;
import logic.people.Participant;
import logic.people.Teacher;
import model.UserGetInfoHandler;
import model.UserSetInfoHandler;
import utils.ControlledScreen;
import utils.ScreenController;

import java.io.File;
import java.io.IOException;

public class TeacherDashboard implements ControlledScreen {

    // UI Elemenst
    public ImageView avatar;
    public Label name;
    public VBox examsBox;
    public VBox chatsBox;
    public VBox settingsBox;
    public ImageView examsIcon;
    public ImageView chatsIcon;
    public ImageView settingsIcon;
    public Label exams;
    public Label chats;
    public Label settings;
    public HBox contentLayout;

    // Sub components
    private VBox examsLayout;
    private HBox examsInfoLayout;
    private VBox examResultLayout;
    private HBox chatsLayout;
    private VBox settingsLayout;
    private ExamsSection examsSectionController;
    private ExamInfo examsInfoController;
    private ExamResults examResultsController;
    private Chats chatsController;
    private Settings settingsController;

    // sidebar icons
    private Image exams_on;
    private Image exams_off;
    private Image chats_on;
    private Image chats_off;
    private Image settings_on;
    private Image settings_off;

    // database connection
    private UserGetInfoHandler userGetInfoHandler = new UserGetInfoHandler();
    private UserSetInfoHandler userSetInfoHandler = new UserSetInfoHandler();

    private ScreenController sc; // screen transition
    public Teacher user; // User
    private String showPanel = "exams"; // Current State
    private ChatClient client;

    @FXML
    public void initialize() {

        File file = new File("src/assets/images/exam_on.png");
        exams_on = new Image(file.toURI().toString());

        file = new File("src/assets/images/exam_off.png");
        exams_off = new Image(file.toURI().toString());

        file = new File("src/assets/images/chat_on.png");
        chats_on = new Image(file.toURI().toString());

        file = new File("src/assets/images/chat_off.png");
        chats_off = new Image(file.toURI().toString());

        file = new File("src/assets/images/settings_on.png");
        settings_on = new Image(file.toURI().toString());

        file = new File("src/assets/images/settings_off.png");
        settings_off = new Image(file.toURI().toString());

        examsBox.setOnMouseClicked((e) -> setPanel("exams"));

        chatsBox.setOnMouseClicked((e) -> setPanel("chats"));

        settingsBox.setOnMouseClicked((e) -> setPanel("settings"));

    }

    public void setUser(Teacher user) {

        this.user = user;
        client.introduce(user.getId().toString());
        name.setText(user.getFirstName() + " " + user.getLastName());
        Image image = new Image(user.getAvatar().toURI().toString());
        avatar.setImage(image);
        renderExams();

    }

    @Override
    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }

    public void setPanel(String panel) {

        offCurrentPanel();
        showPanel = panel;

        switch (panel) {
            case "exams" -> {
                examsBox.setStyle("-fx-background-color: #1BA098;" + "-fx-background-radius: 10%");
                examsBox.setPrefWidth(144);
                examsBox.setPrefHeight(126);
                examsIcon.setImage(exams_on);
                exams.setTextFill(Color.valueOf("#fff"));
                renderExams();
            }
            case "chats" -> {
                chatsBox.setStyle("-fx-background-color: #1BA098;" + "-fx-background-radius: 10%");
                chatsBox.setPrefWidth(144);
                chatsBox.setPrefHeight(126);
                chatsIcon.setImage(chats_on);
                chats.setTextFill(Color.valueOf("#fff"));
                renderChats();
            }
            case "settings" -> {
                settingsBox.setStyle("-fx-background-color: #1BA098;" + "-fx-background-radius: 10%");
                settingsBox.setPrefWidth(144);
                settingsBox.setPrefHeight(126);
                settingsIcon.setImage(settings_on);
                settings.setTextFill(Color.valueOf("fff"));
                renderSettings();
            }
        }

    }

    // remove current panel from screen
    private void offCurrentPanel() {

        switch (showPanel) {
            case "exams" -> {
                examsBox.setStyle("-fx-background-color: #fff;" + "-fx-background-radius: 10%");
                examsBox.setPrefWidth(120);
                examsBox.setPrefHeight(105);
                examsIcon.setImage(exams_off);
                exams.setTextFill(Color.valueOf("#425C5A"));
                contentLayout.getChildren().removeAll(examsLayout);
            }
            case "chats" -> {
                chatsBox.setStyle("-fx-background-color: #fff;" + "-fx-background-radius: 10%");
                chatsBox.setPrefWidth(120);
                chatsBox.setPrefHeight(105);
                chatsIcon.setImage(chats_off);
                chats.setTextFill(Color.valueOf("#425C5A"));
                contentLayout.getChildren().removeAll(chatsLayout);
            }
            case "settings" -> {
                settingsBox.setStyle("-fx-background-color: #fff;" + "-fx-background-radius: 10%");
                settingsBox.setPrefWidth(120);
                settingsBox.setPrefHeight(105);
                settingsIcon.setImage(settings_off);
                settings.setTextFill(Color.valueOf("#425C5A"));
                contentLayout.getChildren().removeAll(settingsLayout);
            }
            case "exam" -> {
                examsBox.setStyle("-fx-background-color: #fff;" + "-fx-background-radius: 10%");
                examsBox.setPrefWidth(120);
                examsBox.setPrefHeight(105);
                examsIcon.setImage(exams_off);
                exams.setTextFill(Color.valueOf("#425C5A"));
                contentLayout.getChildren().removeAll(examsInfoLayout);
            }
            case "examResult" -> {
                examsBox.setStyle("-fx-background-color: #fff;" + "-fx-background-radius: 10%");
                examsBox.setPrefWidth(120);
                examsBox.setPrefHeight(105);
                examsIcon.setImage(exams_off);
                exams.setTextFill(Color.valueOf("#425C5A"));
                contentLayout.getChildren().removeAll(examResultLayout);
            }

        }

    }

    public void renderExams() {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("exams/examsSection.fxml"));
            examsLayout = loader.load();
            examsSectionController = (ExamsSection) loader.getController();
            examsSectionController.setScreen(sc);
            examsSectionController.setTd(this);
            examsSectionController.setUser(user);
            examsSectionController.setUserInfo(user.getExams());
            contentLayout.getChildren().add(examsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void renderChats() {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("chats/chats.fxml"));
            chatsLayout = loader.load();
            chatsController = loader.getController();
            chatsController.setClient(client);
            chatsController.setUser(user);
            contentLayout.getChildren().add(chatsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void renderSettings() {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("settings/settings.fxml"));
            settingsLayout = loader.load();
            settingsController = loader.getController();
            settingsController.setUser(user);
            settingsController.setFather(this);
            contentLayout.getChildren().add(settingsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void renderExam(Exam exam, boolean outdated) {
        offCurrentPanel();
        showPanel = "exam";

        examsBox.setStyle("-fx-background-color: #1BA098;" + "-fx-background-radius: 10%");
        examsBox.setPrefWidth(144);
        examsBox.setPrefHeight(126);
        examsIcon.setImage(exams_on);
        exams.setTextFill(Color.valueOf("#fff"));

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("exams/examInfo/examInfo.fxml"));
            examsInfoLayout = loader.load();
            examsInfoController = loader.getController();
            examsInfoController.setTd(this);
            examsInfoController.setExam(exam, outdated);
            contentLayout.getChildren().add(examsInfoLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void renderNewExam() {
        renderExam(null, false);
    }

    public void renderExamResults(Exam exam) {
        offCurrentPanel();
        showPanel = "examResult";

        examsBox.setStyle("-fx-background-color: #1BA098;" + "-fx-background-radius: 10%");
        examsBox.setPrefWidth(144);
        examsBox.setPrefHeight(126);
        examsIcon.setImage(exams_on);
        exams.setTextFill(Color.valueOf("#fff"));

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("exams/results/examResults.fxml"));
            examResultLayout = loader.load();
            examResultsController = loader.getController();
            examResultsController.setTd(this);
            examResultsController.setUser(user);
            examResultsController.setExam(exam);
            contentLayout.getChildren().add(examResultLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveExam(Exam exam) {
        user.getExams().add(exam);
        setPanel("exams");
        userSetInfoHandler.addExam(user, exam);
        client.send("newExam " + exam.getId());
    }

    public void editExam(Exam exam) {
        setPanel("exams");
        System.out.println(exam.getTitle());
        userSetInfoHandler.updateExam(exam);
        client.send("updateExam " + exam.getId());
    }

    public void update() {
        name.setText(user.getFirstName() + " " + user.getLastName());
        Image iv = new Image(user.getAvatar().toURI().toString());
        avatar.setImage(iv);
        setPanel("exams");
    }

    public void logout(MouseEvent mouseEvent) {
        sc.setLoginScreen();
    }

    public void setClient(ChatClient client) {
        this.client = client;
    }
}
