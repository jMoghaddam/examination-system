package components.dashboard.teacher.chats;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import logic.chat.Member;
import logic.people.Person;
import logic.people.Teacher;

public class MemberCard {

    public Label title;
    public Label name;
    public Circle statusCircle;
    private Person user;
    private Member member;

    public void setUser(Person user, Member member) {
        this.user = user;
        this.member = member;
        if (user instanceof Teacher) title.setText("استاد");
        else title.setText("دانشجو");
        name.setText(user.getFirstName() + " " + user.getLastName());

        if (member.getStatus() == 1) statusCircle.setFill(Color.valueOf("#1BA098"));
    }
}
