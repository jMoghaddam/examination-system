package components.dashboard.teacher.chats;

import chat.ChatClient;
import chat.UserStatusListener;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import logic.chat.Member;
import logic.chat.Message;
import logic.exam.Exam;
import logic.people.Person;
import logic.people.Student;
import logic.people.Teacher;
import model.UserGetInfoHandler;

import java.io.File;
import java.io.IOException;

public class Chats {

    public VBox examsList;
    public VBox memebrsList;
    public VBox messages;
    public TextField msgField;
    public ImageView sendBtn;
    public ScrollPane sc;
    public HBox father;
    private Image ion;
    private Image ioff;
    private Person user;
    private Exam activeExam;
    private Member member;
    private ChatClient client;
    private UserGetInfoHandler userGetInfoHandler = new UserGetInfoHandler();

    public void setUser(Person user) {
        this.user = user;

        if (user.getExams().size() > 0) activeExam = user.getExams().get(0);
        else return;
        if (user instanceof Teacher) {
            this.member = activeExam.getGroup().getMemberById(user.getId().toString());
            for (Exam e : user.getExams()) {
                for (Member m : e.getGroup().getMembers()) {
                    if (m.getId().equals(user.getId().toString())) m.setStatus(1);
                }
            }
        }
        else {
            Student s = (Student) user;
            this.member = activeExam.getGroup().getMemberById(s.getStudentId());
            for (Exam e : user.getExams()) {
                for (Member m : e.getGroup().getMembers()) {
                    if (m.getId().equals(s.getStudentId())) m.setStatus(1);
                }
            }
        }
        renderExamList();
        renderMembers();
        renderMessages();

        client.setUserStatusListener(new UserStatusListener() {
            @Override
            public void online(String id) {
                for (Member member : activeExam.getGroup().getMembers()) {
                   if (member.getId().equals(id)) {
                       member.setStatus(1);
                   }
                }
                renderMembers();
            }

            @Override
            public void offline(String id) {
                for (Member member : activeExam.getGroup().getMembers()) {
                    if (member.getId().equals(id)) {
                        member.setStatus(0);
                    }
                }
                renderMembers();
            }
        });

        client.setMessageListener((hash, from, time, text) -> {

            for (Exam e : user.getExams()) {
                if (hash.equals(e.getGroup().getHash())) {
                    Message message = new Message();
                    Member m = new Member();
                    m.setId(from);
                    message.setFrom(m);
                    message.setTime(time);
                    message.setText(text);
                    e.getGroup().getMessages().add(message);
                    renderMessages();
                    break;
                }
            }

        });

        father.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ENTER) {
                send();
            }
        });

        sendBtn.setOnMouseClicked((e) -> {
            send();
        });

        messages.heightProperty().addListener(observable -> sc.setVvalue(1D));

        File on = new File("src/assets/images/send_on.png");
        ion = new Image(on.toURI().toString());

        File off = new File("src/assets/images/send_off.png");
        ioff = new Image(off.toURI().toString());
        sendBtn.setImage(ioff);

        msgField.textProperty().addListener(((observableValue, oldValue, newValue) -> {
            if (newValue.length() == 0) {
                sendBtn.setImage(ioff);
            } else {
                if (oldValue.length() == 0) {
                    sendBtn.setImage(ion);
                }
            }
        }));

    }

    private void renderExamList() {

        examsList.getChildren().clear();
        for (Exam exam : user.getExams()) {
            HBox hBox = new HBox();
            hBox.setPadding(new Insets(15, 0, 15, 0));
            hBox.setCursor(Cursor.HAND);
            hBox.setAlignment(Pos.CENTER);
            if (exam == activeExam) {
                hBox.setStyle("-fx-background-color: #DEB992");
            } else {
                hBox.setStyle("-fx-background-color: #fff");
            }
            Label l = new Label(exam.getTitle());
            l.setStyle("-fx-font-family: 'Vazir Bold';" + "-fx-text-fill: #425C5A;" + "-fx-font-size: 18px");
            hBox.getChildren().add(l);
            examsList.getChildren().add(hBox);

            hBox.setOnMouseClicked((e) -> {
                setActiveExam(exam);
            });
        }

    }
    
    private void renderMembers() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                memebrsList.getChildren().clear();
                for (Member member : activeExam.getGroup().getMembers()) {
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("memberCard.fxml"));
                        HBox ml = loader.load();
                        MemberCard mc = loader.getController();

                        if (member.getId().equals(user.getId().toString())){
                            mc.setUser(user, member);
                        } else {
                            Person p = activeExam.getUserById(member.getId());
                            if (p != null) mc.setUser(p, member);
                            else {
                                mc.setUser(userGetInfoHandler.getTeacher(member.getId()), member);
                            }
                        }

                        memebrsList.getChildren().add(ml);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void renderMessages() {

        Platform.runLater(new Runnable() {
            @Override public void run() {
                messages.getChildren().clear();
                for (Message message : activeExam.getGroup().getMessages()) {
                    if (message.getFrom().getId().equals(member.getId())) {
                        try {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("sentCard.fxml"));
                            HBox scl = loader.load();
                            SentCard scc = loader.getController();
                            scc.setMessage(message);
                            messages.getChildren().add(scl);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("recievedCard.fxml"));
                            HBox rcl = loader.load();
                            ReceivedCard rcc = loader.getController();
                            Person p = activeExam.getUserById(message.getFrom().getId());
                            if (p != null) rcc.setMessage(message, activeExam.getUserById(message.getFrom().getId()));
                            else {
                                rcc.setMessage(message, userGetInfoHandler.getTeacher(message.getFrom().getId()));
                            }
                            messages.getChildren().add(rcl);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                sc.layout();
                sc.setVvalue(1.0);
            }
        });
    }

    private void setActiveExam(Exam exam) {
        activeExam = exam;
        renderExamList();
        renderMembers();
        renderMessages();
    }

    public void send() {
        if (msgField.getText().length() != 0) {
            if (user instanceof Teacher) {
                client.send("msg " + activeExam.getGroup().getHash() + " " + user.getId() + " " + msgField.getText());
            } else {
                Student s = (Student) user;
                client.send("msg " + activeExam.getGroup().getHash() + " " + s.getStudentId() + " " + msgField.getText());
            }
            msgField.setText("");
            sendBtn.setImage(ioff);
        }
    }

    public void setClient(ChatClient client) {
        this.client = client;
    }
}
