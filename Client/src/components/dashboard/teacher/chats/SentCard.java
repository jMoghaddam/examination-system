package components.dashboard.teacher.chats;

import javafx.scene.control.Label;
import logic.chat.Message;

public class SentCard {

    public Label time;
    public Label text;
    private Message message;

    public void setMessage(Message message) {
        this.message = message;

        time.setText(message.getTime());
        text.setText(message.getText());
    }
}
