package components.dashboard.teacher.chats;

import javafx.scene.control.Label;
import logic.chat.Message;
import logic.people.Person;

public class ReceivedCard {

    public Label name;
    public Label text;
    public Label time;

    public void setMessage(Message message, Person user) {

        name.setText(user.getFirstName() + " " + user.getLastName());
        time.setText(message.getTime());
        text.setText(message.getText());

    }
}
