package components.dashboard.student.results;

import components.dashboard.student.StudentDashboard;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.exam.Exam;
import logic.exam.answer.Answer;
import logic.people.Participant;
import logic.people.Student;

public class ExamResult {

    public Label date;
    public Label time;
    public Label duration;
    public Label title;
    public Label grade;
    public Label avg;
    public Label rank;
    public HBox queue;
    private StudentDashboard sd;
    private Participant participant;
    private Exam exam;
    private Student user;

    public void setSd(StudentDashboard sd) {
        this.sd = sd;
    }

    public void setExam(Exam exam) {
        this.exam = exam;

        for (Participant p : exam.getParticipants()) {
            if (p.getStudentId().equals(user.getStudentId())) {
                participant = p;
                break;
            }
        }

        title.setText(exam.getTitle());
        if (exam.getDuration() != null) duration.setText(exam.getDuration().toString());
        time.setText(exam.getStartTime().toString());
        date.setText(exam.getStartDate().toString());
        grade.setText(String.valueOf(participant.getGrade()));

        for (Answer a : participant.getAnswers()) {
            VBox v = new VBox();
            v.setAlignment(Pos.CENTER);
            v.setSpacing(10);
            Label n = new Label(String.valueOf(a.getNumber()));
            n.setStyle("-fx-font-size: 14px;" + "-fx-font-family: 'Vazir Bold'");
            n.setTextFill(Color.valueOf("#707070"));
            Label g = new Label(String.valueOf(a.getGrade()));
            g.setStyle("-fx-font-size: 24px;" + "-fx-font-family: 'Vazir Bold'");
            g.setTextFill(Color.valueOf("#1BA098"));
            v.getChildren().addAll(n, g);
            queue.getChildren().add(v);
        }

        System.out.println(exam.getResult().getAvg());
        avg.setText(String.valueOf(exam.getResult().getAvg()));
        rank.setText(String.valueOf(participant.getRank()));

    }

    public void setP(Student user) {
        this.user = user;
    }

    public void review(ActionEvent actionEvent) {

    }

    public void goBack(ActionEvent actionEvent) {
        sd.renderExams();
    }
}
