package components.dashboard.student.exams.mainExam;

import components.dashboard.student.StudentDashboard;
import components.dashboard.teacher.TeacherDashboard;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import logic.exam.Exam;
import logic.people.Student;
import model.UserGetInfoHandler;
import utils.ControlledScreen;
import utils.ScreenController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class MainExamSection implements ControlledScreen {

    public Label all;
    public Label ungraded;
    private final HashMap<Integer, VBox> layouts = new HashMap<>();
    private final HashMap<Integer, ExamCard> controllers = new HashMap<Integer, ExamCard>();
    public FlowPane flow;

    private UserGetInfoHandler userGetInfoHandler = new UserGetInfoHandler();
    private ArrayList<Exam> exams;
    private StudentDashboard sd;
    private ScreenController sc;
    private Student user;

    public void setSd(StudentDashboard sd) {
        this.sd = sd;
    }

    public void setUserInfo(Student user) {
        this.user = user;
        this.exams = user.getExams();
        renderExamCards();
    }

    private void renderExamCards() {

        int i = 0;
        for (Exam exam : exams) {

            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("examCard.fxml"));
                VBox holderLayout = loader.load();
                ExamCard holderController = loader.getController();
                holderController.setScreen(sc);
                holderController.setSd(sd);
                holderController.setExamInfo(exam, user);
                holderController.setMainExamSection(this);
                controllers.put(i, holderController);
                flow.getChildren().add(holderLayout);
                layouts.put(i, holderLayout);
            } catch (IOException e) {
                e.printStackTrace();
            }

            i++;

        }

    }

    @Override
    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }
}
