package components.dashboard.student.exams.mainExam;

import components.dashboard.student.StudentDashboard;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import logic.Date;
import logic.Time;
import logic.exam.Exam;
import logic.exam.question.Question;
import logic.people.Participant;
import logic.people.Student;
import utils.ControlledScreen;
import utils.ScreenController;

import java.time.Duration;
import java.time.LocalDateTime;

public class ExamCard implements ControlledScreen {

    public Label title;
    public Label date;
    public Label time;
    public Button actionButton;
    public VBox examCardLayout;
    public Label duration;

    private MainExamSection mainExamSection;
    private Exam exam;
    private String condition = "pending";
    private ScreenController sc;
    private StudentDashboard sd;
    private Student user;

    public void setExamInfo(Exam exam, Student user) {
        this.user = user;
        this.exam = exam;
        title.setText(exam.getTitle());
        time.setText(exam.getStartTime().toString());
        date.setText(exam.getStartDate().toString());
        if (exam.getDuration() != null) duration.setText(exam.getDuration().toString());

        LocalDateTime ldt = LocalDateTime.now();
        Date d = new Date(ldt.getYear(), ldt.getMonthValue(), ldt.getDayOfMonth());

        if (Date.isBigger(exam.getStartDate(), d) > 0) {
            condition = "pending";
        } else if (Date.isBigger(exam.getStartDate(), d) == 0) {

            Time ts = new Time();

            if (exam.getDuration() == null) {
                int dhour = 0;
                int dmin = 0;
                for (Question q : exam.getQuestions()) {
                    dhour += q.getDuration().getHour();
                    dmin += q.getDuration().getMin();
                }

                ts.setHour(exam.getStartTime().getHour() + dhour);
                ts.setMin(exam.getStartTime().getMin() + dmin);
            } else {
                int h = exam.getStartTime().getHour() + exam.getDuration().getHour();
                int m = exam.getStartTime().getMin() + exam.getDuration().getMin();
                while (m > 60) {
                    m -= 60;
                    h++;
                }
                ts.setHour(h);
                ts.setMin(m);
            }

            Time t = new Time();
            t.setHour(ldt.getHour());
            t.setMin(ldt.getMinute());


            if (Time.isBigger(exam.getStartTime(), t) > 0) {
                condition = "pending";
            } else if (Time.isBigger(t, ts) > 0) {
                System.out.println(t);
                System.out.println(ts);
                condition = "grading";
            } else {
                condition = "participate";
            }

        } else {
            condition = "grading";
        }

        for (Participant p : exam.getParticipants()) {
            if (p.getStudentId().equals(user.getStudentId())) {
                if (p.isHasParticipated()) {
                    condition = "grading";
                    break;
                }
            }
        }

        if (exam.getResult().getGraded() == exam.getParticipants().size() * exam.getQuestions().size()) {
            condition = "graded";
        }

        for (Participant p : exam.getParticipants()) {
            if (p.getStudentId().equals(user.getStudentId())) {
                if (p.isBanned()) {
                    condition = "banned";
                    break;
                }
            }
        }

        if (condition.equals("pending")) {
            actionButton.setStyle("-fx-background-color: #707070");
            actionButton.setText("در انتظار");
        }

        if (condition.equals("participate")) {
            actionButton.setStyle("-fx-background-color: #1BA098");
            actionButton.setText("شرکت");
            actionButton.setOnMouseClicked((e) -> {
                sd.takeExam(exam);
            });
        }

        if (condition.equals("grading")) {
            actionButton.setStyle("-fx-background-color: #051622");
            actionButton.setText("در حال تصحیح");
        }

        if (condition.equals("banned")) {
            actionButton.setStyle("-fx-background-color: red");
            actionButton.setText("محروم");
        }

        if (condition.equals("graded")) {
            actionButton.setStyle("-fx-background-color: #DEB992");
            actionButton.setText("نتایج");
            actionButton.setOnMouseClicked((e) -> {
                sd.renderExamResult(exam);
            });
        }

    }

    public void setMainExamSection(MainExamSection mainExamSection) {
        this.mainExamSection = mainExamSection;
    }

    @Override
    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }

    public void setSd(StudentDashboard sd) {
        this.sd = sd;
    }
}
