package components.dashboard.student.exams;

import components.dashboard.student.StudentDashboard;
import components.dashboard.student.exams.mainExam.MainExamSection;
import components.dashboard.student.exams.results.ResultsSection;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.exam.Exam;
import logic.people.Student;
import model.UserGetInfoHandler;
import utils.ControlledScreen;
import utils.ScreenController;

import java.io.IOException;
import java.util.ArrayList;

public class StudentExamSection implements ControlledScreen {

    public VBox contentLayout;
    public Label examsOption;
    private VBox mainExamsLayout;
    private HBox resultsLayout;
    private MainExamSection mainExamSectionController;
    private ResultsSection resultsSectionController;

    private UserGetInfoHandler userGetInfoHandler = new UserGetInfoHandler();
    private ArrayList<Exam> exams;
    private StudentDashboard sd;
    private String section = "main";
    private ScreenController sc;
    private Student user;

    @FXML
    public void initialize() {

        examsOption.setOnMouseClicked((e) -> setSection("main"));

    }

    private void setSection(String section) {

        offCurrentSection();
        this.section = section;

        if ("main".equals(section)) {
            examsOption.setTextFill(Color.valueOf("#1BA098"));
            renderExamsSection();
        }

    }

    public void setUserInfo(Student user) {
        this.user = user;
        this.exams = user.getExams();
        renderExamsSection();
    }

    public void setSd(StudentDashboard sd) {
        this.sd = sd;
    }

    private void offCurrentSection() {

        if ("main".equals(section)) {
            examsOption.setTextFill(Color.valueOf("#707070"));
            contentLayout.getChildren().removeAll(mainExamsLayout);
        }

    }

    private void renderExamsSection() {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("mainExam/mainExamSection.fxml"));
            mainExamsLayout = loader.load();
            mainExamSectionController = loader.getController();
            mainExamSectionController.setScreen(sc);
            mainExamSectionController.setSd(sd);
            mainExamSectionController.setUserInfo(user);
            contentLayout.getChildren().add(mainExamsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }
}
