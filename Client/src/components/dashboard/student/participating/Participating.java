package components.dashboard.student.participating;

import chat.ChatClient;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.exam.Exam;
import logic.people.Participant;
import logic.people.Student;
import org.bson.types.ObjectId;
import utils.ScreenController;

import java.io.IOException;

public class Participating {

    public HBox contentLayout;
    private VBox introLayout;
    private VBox wholeTimeSheetLayout;
    private VBox partTimeSheetLayout;
    private VBox votingLayout;

    private ParticipateIntro participateIntro;
    private WholeTimeSheet wholeTimeSheet;
    private PartTimeSheet partTimeSheet;
    private Voting voting;

    private ScreenController sc;
    private Exam exam;
    private Participant participant;
    private String state = "intro";
    private Student user;
    private ChatClient client;

    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }

    public void setInfo(Exam exam, Participant participant) {
        this.exam = exam;
        this.participant = participant;
        renderIntro();
    }

    private void offCurrentPanel() {

        switch (state) {
            case "intro" -> {
                contentLayout.getChildren().removeAll(introLayout);
            }
            case "whole sheet" -> {
                contentLayout.getChildren().removeAll(wholeTimeSheetLayout);
            }
            case "part sheet" -> {
                contentLayout.getChildren().removeAll(partTimeSheetLayout);
            }
            case "voting" -> {
                contentLayout.getChildren().removeAll(votingLayout);
            }

        }

    }

    private void renderIntro() {

        offCurrentPanel();
        state = "intro";

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("participateIntro.fxml"));
            introLayout = loader.load();
            participateIntro = loader.getController();
            participateIntro.setSd(this);
            contentLayout.getChildren().add(introLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void renderWholeSheet() {

        offCurrentPanel();
        state = "whole sheet";

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("wholeTimeSheet.fxml"));
            wholeTimeSheetLayout = loader.load();
            wholeTimeSheet = loader.getController();
            wholeTimeSheet.setSd(this);
            wholeTimeSheet.setInfo(exam, participant);
            contentLayout.getChildren().add(wholeTimeSheetLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void renderPartSheet() {

        offCurrentPanel();
        state = "part sheet";

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("partTimeSheet.fxml"));
            partTimeSheetLayout = loader.load();
            partTimeSheet = loader.getController();
            partTimeSheet.setSd(this);
            partTimeSheet.setInfo(exam, participant);
            contentLayout.getChildren().add(partTimeSheetLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void renderVoting() {

        offCurrentPanel();
        state = "voting";

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("voting.fxml"));
            votingLayout = loader.load();
            voting = loader.getController();
            voting.setSd(this);
            voting.setExam(exam);
            contentLayout.getChildren().add(votingLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void startExam() {

        if (exam.getDuration() == null) {
            renderPartSheet();
        } else {
            renderWholeSheet();
        }

    }

    public void setUser(Student user) {
        this.user = user;
    }

    public void goHome() {
        sc.setStudentDashboardScreen(user);
    }

    public void setClient(ChatClient client) {
        this.client = client;
    }
}
