package components.dashboard.student.participating;

import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import logic.exam.answer.DescriptiveAnswer;
import logic.exam.question.DescriptiveQuestion;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class DescriptiveQuestionBox extends QuestionBox {


    public Label generalInfo;
    public Label body;
    public Button imgUpload;
    public Button fUpload;
    public VBox optionsLayout;
    public TextArea text;
    private DescriptiveQuestion setQuestion;
    private FileChooser imgChooser = new FileChooser();
    private FileChooser fileChooser = new FileChooser();
    ImageView imageUploaded = new ImageView();
    ImageView fileUploaded = new ImageView();
    public DescriptiveAnswer descriptiveAnswer = new DescriptiveAnswer();

    public void setQuestion(DescriptiveQuestion question) {
        this.setQuestion = question;

        generalInfo.setText(question.getTitle() + " - " + question.getPoint() + " نمره");
        body.setText(question.getBody());

        text.setText("");
        descriptiveAnswer.setNumber(question.getNumber());

        File f = new File("src/assets/images/lname.png");
        Image i =  new Image(f.toURI().toString());
        imageUploaded.setImage(i);
        imageUploaded.setFitWidth(40);
        imageUploaded.setFitHeight(40);
        imageUploaded.setCursor(Cursor.HAND);

        imageUploaded.setOnMouseClicked((e) -> {
            try {
                Desktop.getDesktop().open(descriptiveAnswer.getImage());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        f = new File("src/assets/images/question.png");
        i =  new Image(f.toURI().toString());
        fileUploaded.setImage(i);
        fileUploaded.setFitWidth(40);
        fileUploaded.setFitHeight(40);
        fileUploaded.setCursor(Cursor.HAND);

        fileUploaded.setOnMouseClicked((e) -> {
            try {
                Desktop.getDesktop().open(descriptiveAnswer.getFile());
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        if (!question.isImageAllowed()) {
            optionsLayout.getChildren().removeAll(imgUpload);
            optionsLayout.getChildren().removeAll(imageUploaded);
        }

        if (!question.isFileAllowed()) {
            optionsLayout.getChildren().removeAll(fUpload);
            optionsLayout.getChildren().removeAll(fileUploaded);
        }

        imgUpload.setOnMouseClicked((e) -> {
            descriptiveAnswer.setImage(imgChooser.showOpenDialog(new Stage()));
            int btnIndex = optionsLayout.getChildren().indexOf(imgUpload);
            optionsLayout.getChildren().add(btnIndex + 1, imageUploaded);
        });

        fUpload.setOnMouseClicked((e) -> {
            descriptiveAnswer.setFile(fileChooser.showOpenDialog(new Stage()));
            int btnIndex = optionsLayout.getChildren().indexOf(fUpload);
            optionsLayout.getChildren().add(btnIndex + 1, fileUploaded);
        });

        imgChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.jpeg", "*.png")
        );

    }


}
