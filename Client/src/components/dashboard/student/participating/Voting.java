package components.dashboard.student.participating;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import logic.exam.Exam;
import model.UserSetInfoHandler;

public class Voting {

    public Label hard;
    public Label normal;
    public Label easy;
    public Button end;
    private Participating par;
    private Exam exam;
    private UserSetInfoHandler userSetInfoHandler = new UserSetInfoHandler();

    public void setSd(Participating participating) {
        this.par = participating;
    }

    public void setExam(Exam exam) {
        this.exam = exam;

        easy.setOnMouseClicked((e) -> {
            exam.getResult().simpleInc();
            userSetInfoHandler.saveStats(exam);
            par.goHome();
        });

        normal.setOnMouseClicked((e) -> {
            exam.getResult().normalInc();
            userSetInfoHandler.saveStats(exam);
            par.goHome();
        });

        hard.setOnMouseClicked((e) -> {
            exam.getResult().hardInc();
            userSetInfoHandler.saveStats(exam);
            par.goHome();
        });

    }
}
