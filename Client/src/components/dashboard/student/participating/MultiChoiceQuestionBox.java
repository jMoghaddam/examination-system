package components.dashboard.student.participating;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import logic.exam.answer.MultiChoiceAnswer;
import logic.exam.question.Choice;
import logic.exam.question.MultiChoiceQuestion;

import java.util.HashMap;

public class MultiChoiceQuestionBox extends QuestionBox {

    public FlowPane choicesLayout;
    public Label general;
    public Label body;
    private MultiChoiceQuestion question;
    private HashMap<Integer, HBox> cs = new HashMap<>();
    private HashMap<Integer, Rectangle> rs = new HashMap<>();
    public MultiChoiceAnswer multiChoiceAnswer = new MultiChoiceAnswer();

    public void setQuestion(MultiChoiceQuestion question) {
        this.question = question;

        general.setText(question.getTitle() + " - " + question.getPoint() + " نمره");
        body.setText(question.getBody());

        multiChoiceAnswer.setNumber(question.getNumber());
        multiChoiceAnswer.setAnswer(question.getChoices().get(0));

        choicesLayout.getChildren().clear();
        cs.clear();
        rs.clear();
        for (Choice choice : question.getChoices()) {

            HBox h = new HBox();
            h.setAlignment(Pos.CENTER);
            h.setSpacing(20);
            h.setPadding(new Insets(0, 50, 0, 0));
            Rectangle rect = new Rectangle();
            rect.setWidth(25);
            rect.setHeight(25);
            if (choice.getNumber() == 1) rect.setFill(Color.valueOf("#DEB992"));
            else rect.setFill(Color.valueOf("#fff"));
            rs.put(choice.getNumber(), rect);

            Label l = new Label();
            l.setTextFill(Color.valueOf("#fff"));
            l.setStyle("-fx-font-family: 'Vazir';" + "-fx-font-size: 18px");
            l.setText(choice.getText());
            h.getChildren().add(rect);
            h.getChildren().add(l);
            choicesLayout.getChildren().add(h);

            h.setOnMouseClicked((e) -> {

                int chosen = 0;
                for (Choice c : question.getChoices()) {
                    if (h == cs.get(c.getNumber())) {
                        rs.get(c.getNumber()).setFill(Color.valueOf("#DEB992"));
                        chosen = c.getNumber();
                        multiChoiceAnswer.setAnswer(c);
                        break;
                    }
                }

                for (Choice c : question.getChoices()) {
                    if (c.getNumber() != chosen) {
                        rs.get(c.getNumber()).setFill(Color.valueOf("#fff"));
                    }
                }

            });

            cs.put(choice.getNumber(), h);

        }
    }
}
