package components.dashboard.student.participating;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import logic.exam.answer.TrueFalseAnswer;
import logic.exam.question.TrueFalseQuestion;

public class TrueFalseQuestionBox extends QuestionBox {

    public Label selectFalse;
    public Label selectTrue;
    public Label general;
    public Label body;
    private TrueFalseQuestion question;
    public TrueFalseAnswer trueFalseAnswer = new TrueFalseAnswer();

    public void setQuestion(TrueFalseQuestion question) {
        this.question = question;

        general.setText(question.getTitle() + " - " + question.getPoint() + " نمره");
        body.setText(question.getBody());

        trueFalseAnswer.setNumber(question.getNumber());

        selectTrue.setOnMouseClicked((e) -> {
            trueFalseAnswer.setAnswer(true);
            selectTrue.setTextFill(Color.valueOf("#DEB992"));
            selectFalse.setTextFill(Color.valueOf("#fff"));
        });

        selectFalse.setOnMouseClicked((e) -> {
            trueFalseAnswer.setAnswer(false);
            selectTrue.setTextFill(Color.valueOf("#fff"));
            selectFalse.setTextFill(Color.valueOf("#DEB992"));
        });
    }
}
