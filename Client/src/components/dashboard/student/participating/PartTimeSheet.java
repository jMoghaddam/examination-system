package components.dashboard.student.participating;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import logic.Time;
import logic.exam.Exam;
import logic.exam.answer.Answer;
import logic.exam.question.DescriptiveQuestion;
import logic.exam.question.MultiChoiceQuestion;
import logic.exam.question.Question;
import logic.exam.question.TrueFalseQuestion;
import logic.people.Participant;
import model.UserSetInfoHandler;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;

public class PartTimeSheet {

    public HBox queue;
    public VBox contentLayout;
    public Button next;
    private Participating sd;
    private Exam exam;
    private Participant participant;
    private int currQ = 1;
    long nowSec = 0;
    ArrayList<Long> durartions = new ArrayList<>();
    private HashMap<Integer, VBox> layouts = new HashMap<>();
    private HashMap<Integer, QuestionBox> controllers = new HashMap<>();
    private UserSetInfoHandler userSetInfoHandler = new UserSetInfoHandler();

    public void setSd(Participating participating) {
        this.sd = participating;
    }

    public void setInfo(Exam exam, Participant participant) {
        this.exam = exam;
        this.participant = participant;

        for (int i : participant.getQuestionQueue()) {
            Label l = new Label(String.valueOf(i));
            l.setTextFill(Color.valueOf("#fff"));
            queue.getChildren().add(l);
        }

        for (Question q : exam.getQuestions()) {
            durartions.add((long) (q.getDuration().getHour() * 3600 + q.getDuration().getMin() * 60));
        }

        int syear = exam.getStartDate().getYear();
        int smonth = exam.getStartDate().getMonth();
        int sday = exam.getStartDate().getDay();
        int shour = exam.getStartTime().getHour();
        int smin = exam.getStartTime().getMin();

        LocalDateTime l = LocalDateTime.of(syear, smonth, sday, shour, smin);

        LocalDateTime now = LocalDateTime.now();
        nowSec = now.toEpochSecond(ZoneOffset.MAX) - l.toEpochSecond(ZoneOffset.MAX);

        int k = 1;
        while (true) {

            if (k - 1 == exam.getQuestions().size()) {
                endExam();
                break;
            }

            if (nowSec < summer(k)) {
                renderQuestion(exam.getQuestions().get(k - 1));
                break;
            }

            k++;
        }

        Timeline fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                nowSec++;
                if (nowSec < summer(currQ)) {
                    next.setText(Time.sToTime(summer(currQ) - nowSec).toVeryString());
                } else {
                    if (currQ < exam.getQuestions().size()) {
                        currQ++;
                        renderQuestion(exam.getQuestions().get(currQ - 1));
                        next.setText(String.valueOf(durartions.get(currQ - 1)));
                    } else {
                        endExam();
                    }
                }

            }
        }));
        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
        fiveSecondsWonder.play();

    }

    private long summer(int s) {

        long result = 0;
        for (int i = 0; i < s; i++) {
            result += durartions.get(i);
        }

        return result;

    }

    private void endExam() {

        boolean over = true;
        double grade = 0.0;
        ArrayList<Answer> answers = new ArrayList<>();
        for (int i : participant.getQuestionQueue()) {
            if (exam.getQuestions().get(i-1) instanceof DescriptiveQuestion) {
                DescriptiveQuestionBox d = (DescriptiveQuestionBox) controllers.get(i);
                d.descriptiveAnswer.setText(d.text.getText());
                answers.add(d.descriptiveAnswer);
                over = false;
            }
            if (exam.getQuestions().get(i-1) instanceof TrueFalseQuestion) {
                TrueFalseQuestion tfq = (TrueFalseQuestion) exam.getQuestions().get(i-1);
                TrueFalseQuestionBox d = (TrueFalseQuestionBox) controllers.get(i);
                if (d.trueFalseAnswer.isAnswer() == tfq.isCorrectAnswer()) {
                    d.trueFalseAnswer.setGrade(tfq.getPoint());
                    grade += tfq.getPoint();
                } else d.trueFalseAnswer.setGrade(0.0);
                answers.add(d.trueFalseAnswer);
                exam.getResult().gradedInc();
            }
            if (exam.getQuestions().get(i-1) instanceof MultiChoiceQuestion) {
                MultiChoiceQuestion mcq = (MultiChoiceQuestion) exam.getQuestions().get(i-1);
                MultiChoiceQuestionBox d = (MultiChoiceQuestionBox) controllers.get(i);
                if (d.multiChoiceAnswer.getAnswer().getNumber() == mcq.getCorrectAnswer().getNumber()) {
                    d.multiChoiceAnswer.setGrade(mcq.getPoint());
                    grade += mcq.getPoint();
                } else d.multiChoiceAnswer.setGrade(0.0);
                answers.add(d.multiChoiceAnswer);
                exam.getResult().gradedInc();
            }

        }

        if (over) {
            participant.setGrade(grade);
        }

        participant.setAnswers(answers);
        participant.setHasParticipated(true);
        userSetInfoHandler.setAnswer(exam, participant, over);
        sd.renderVoting();

    }

    public void renderQuestion(Question question) {
        System.out.println("$$$$$$$");

        contentLayout.getChildren().clear();

        if (question instanceof DescriptiveQuestion) {
            VBox hlayout;
            DescriptiveQuestionBox hcontroller = new DescriptiveQuestionBox();
            try {
                if (!layouts.containsKey(question.getNumber())) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("descriptiveQuestionBox.fxml"));
                    hlayout = loader.load();
                    hcontroller = loader.getController();
                    layouts.put(question.getNumber(), hlayout);
                    controllers.put(question.getNumber(), hcontroller);
                } else {
                    hlayout = layouts.get(question.getNumber());
                    hcontroller = (DescriptiveQuestionBox) controllers.get(question.getNumber());
                }
                hcontroller.setQuestion((DescriptiveQuestion) question);
                contentLayout.getChildren().add(hlayout);
            } catch (IOException e) {
                System.out.println("run");
            }
        }

        if (question instanceof TrueFalseQuestion) {
            VBox hlayout;
            TrueFalseQuestionBox hcontroller = new TrueFalseQuestionBox();
            try {
                if (!layouts.containsKey(question.getNumber())) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("trueFalseQuestionBox.fxml"));
                    hlayout = loader.load();
                    hcontroller = loader.getController();
                    layouts.put(question.getNumber(), hlayout);
                    controllers.put(question.getNumber(), hcontroller);
                } else {
                    hlayout = layouts.get(question.getNumber());
                    hcontroller = (TrueFalseQuestionBox) controllers.get(question.getNumber());
                }
                hcontroller.setQuestion((TrueFalseQuestion) question);
                contentLayout.getChildren().add(hlayout);
            } catch (IOException e) {
                System.out.println("run");
            }
        }

        if (question instanceof MultiChoiceQuestion) {
            VBox hlayout;
            MultiChoiceQuestionBox hcontroller = new MultiChoiceQuestionBox();
            try {
                if (!layouts.containsKey(question.getNumber())) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("multiChoiceQuestionBox.fxml"));
                    hlayout = loader.load();
                    hcontroller = loader.getController();
                    layouts.put(question.getNumber(), hlayout);
                    controllers.put(question.getNumber(), hcontroller);
                } else {
                    hlayout = layouts.get(question.getNumber());
                    hcontroller = (MultiChoiceQuestionBox) controllers.get(question.getNumber());
                }
                hcontroller.setQuestion((MultiChoiceQuestion) question);
                contentLayout.getChildren().add(hlayout);
            } catch (IOException e) {
                System.out.println("run");
            }
        }

    }

}
