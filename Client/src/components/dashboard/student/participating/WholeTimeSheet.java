package components.dashboard.student.participating;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import logic.Time;
import logic.exam.Exam;
import logic.exam.answer.Answer;
import logic.exam.answer.MultiChoiceAnswer;
import logic.exam.question.DescriptiveQuestion;
import logic.exam.question.MultiChoiceQuestion;
import logic.exam.question.Question;
import logic.exam.question.TrueFalseQuestion;
import logic.people.Participant;
import model.UserSetInfoHandler;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

public class WholeTimeSheet {

    public HBox queue;
    public VBox contentLayout;
    public Button next;
    public Label timer;
    public Button prev;
    private Participating sd;
    private Exam exam;
    private Participant participant;
    private int currQ = 0;
    long left;
    private HashMap<Integer, VBox> layouts = new HashMap<>();
    private HashMap<Integer, QuestionBox> controllers = new HashMap<>();
    private UserSetInfoHandler userSetInfoHandler = new UserSetInfoHandler();

    public void setSd(Participating participating) {
        this.sd = participating;
    }

    public void setInfo(Exam exam, Participant participant) {

        this.exam = exam;
        this.participant = participant;

        int dayH = exam.getStartDate().getDay();
        int hourH = exam.getStartTime().getHour() + exam.getDuration().getHour();
        if (hourH >= 24) {
            dayH++;
            hourH -= 24;
        }
        int minH = exam.getStartTime().getMin() + exam.getDuration().getMin();
        if (minH >= 60) {
            hourH++;
            minH -= 60;
        }

        LocalDateTime end = LocalDateTime.of(exam.getStartDate().getYear(), exam.getStartDate().getMonth(), dayH, hourH, minH);
        LocalDateTime now = LocalDateTime.now();
        left = end.toEpochSecond(ZoneOffset.MAX) - now.toEpochSecond(ZoneOffset.MAX);

        Timeline fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                left--;
                timer.setText(Time.sToTime(left).toVeryString());

                if (left == 0) {
                    endExam();
                }

            }
        }));
        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
        fiveSecondsWonder.play();

        for (Question q : exam.getQuestions()) {
            if (q.getNumber() == participant.getQuestionQueue().get(currQ)) {
                renderQuestion(q);
                break;
            }
        }

        next.setOnMouseClicked((e) -> {
            if (currQ < participant.getQuestionQueue().size() - 1) {
                currQ++;
                for (Question q : exam.getQuestions()) {
                    if (q.getNumber() == participant.getQuestionQueue().get(currQ)) {
                        renderQuestion(q);
                        break;
                    }
                }
            } else {
                endExam();
            }

            if (currQ == participant.getQuestionQueue().size() - 1) {
                next.setText("finish");
            }
        });

        prev.setOnMouseClicked((e) -> {
            if (currQ != 0) {
                currQ--;
                for (Question q : exam.getQuestions()) {
                    if (q.getNumber() == participant.getQuestionQueue().get(currQ)) {
                        renderQuestion(q);
                        break;
                    }
                }
            }
        });

    }

    private void endExam() {

        boolean over = true;
        double grade = 0.0;
        ArrayList<Answer> answers = new ArrayList<>();
        for (int i : participant.getQuestionQueue()) {
            if (exam.getQuestions().get(i-1) instanceof DescriptiveQuestion) {
                DescriptiveQuestionBox d = (DescriptiveQuestionBox) controllers.get(i);
                d.descriptiveAnswer.setText(d.text.getText());
                answers.add(d.descriptiveAnswer);
                over = false;
            }
            if (exam.getQuestions().get(i-1) instanceof TrueFalseQuestion) {
                TrueFalseQuestion tfq = (TrueFalseQuestion) exam.getQuestions().get(i-1);
                TrueFalseQuestionBox d = (TrueFalseQuestionBox) controllers.get(i);
                if (d.trueFalseAnswer.isAnswer() == tfq.isCorrectAnswer()) {
                    d.trueFalseAnswer.setGrade(tfq.getPoint());
                    grade += tfq.getPoint();
                } else d.trueFalseAnswer.setGrade(0.0);
                answers.add(d.trueFalseAnswer);
                exam.getResult().gradedInc();
            }
            if (exam.getQuestions().get(i-1) instanceof MultiChoiceQuestion) {
                MultiChoiceQuestion mcq = (MultiChoiceQuestion) exam.getQuestions().get(i-1);
                MultiChoiceQuestionBox d = (MultiChoiceQuestionBox) controllers.get(i);
                if (d.multiChoiceAnswer.getAnswer().getNumber() == mcq.getCorrectAnswer().getNumber()) {
                    d.multiChoiceAnswer.setGrade(mcq.getPoint());
                    grade += mcq.getPoint();
                } else d.multiChoiceAnswer.setGrade(0.0);
                answers.add(d.multiChoiceAnswer);
                exam.getResult().gradedInc();
            }

        }

        if (over) {
            participant.setGrade(grade);
        }


        participant.setAnswers(answers);
        participant.setHasParticipated(true);
        userSetInfoHandler.setAnswer(exam, participant, over);
        sd.renderVoting();

    }

    public void renderQuestion(Question question) {

        contentLayout.getChildren().clear();

        if (question instanceof DescriptiveQuestion) {
            VBox hlayout;
            DescriptiveQuestionBox hcontroller = new DescriptiveQuestionBox();
            try {
                if (!layouts.containsKey(question.getNumber())) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("descriptiveQuestionBox.fxml"));
                    hlayout = loader.load();
                    hcontroller = loader.getController();
                    layouts.put(question.getNumber(), hlayout);
                    controllers.put(question.getNumber(), hcontroller);
                } else {
                    hlayout = layouts.get(question.getNumber());
                    hcontroller = (DescriptiveQuestionBox) controllers.get(question.getNumber());
                }
                hcontroller.setQuestion((DescriptiveQuestion) question);
                contentLayout.getChildren().add(hlayout);
            } catch (IOException e) {
                System.out.println("run");
            }
        }

        if (question instanceof TrueFalseQuestion) {
            VBox hlayout;
            TrueFalseQuestionBox hcontroller = new TrueFalseQuestionBox();
            try {
                if (!layouts.containsKey(question.getNumber())) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("trueFalseQuestionBox.fxml"));
                    hlayout = loader.load();
                    hcontroller = loader.getController();
                    layouts.put(question.getNumber(), hlayout);
                    controllers.put(question.getNumber(), hcontroller);
                } else {
                    hlayout = layouts.get(question.getNumber());
                    hcontroller = (TrueFalseQuestionBox) controllers.get(question.getNumber());
                }
                hcontroller.setQuestion((TrueFalseQuestion) question);
                contentLayout.getChildren().add(hlayout);
            } catch (IOException e) {
                System.out.println("run");
            }
        }

        if (question instanceof MultiChoiceQuestion) {
            VBox hlayout;
            MultiChoiceQuestionBox hcontroller = new MultiChoiceQuestionBox();
            try {
                if (!layouts.containsKey(question.getNumber())) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("multiChoiceQuestionBox.fxml"));
                    hlayout = loader.load();
                    hcontroller = loader.getController();
                    layouts.put(question.getNumber(), hlayout);
                    controllers.put(question.getNumber(), hcontroller);
                } else {
                    hlayout = layouts.get(question.getNumber());
                    hcontroller = (MultiChoiceQuestionBox) controllers.get(question.getNumber());
                }
                hcontroller.setQuestion((MultiChoiceQuestion) question);
                contentLayout.getChildren().add(hlayout);
            } catch (IOException e) {
                System.out.println("run");
            }
        }

    }

}
