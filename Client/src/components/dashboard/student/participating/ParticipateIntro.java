package components.dashboard.student.participating;

import javafx.event.ActionEvent;

public class ParticipateIntro {

    private Participating sd;

    public void setSd(Participating sd) {
        this.sd = sd;
    }

    public void next(ActionEvent actionEvent) {
        sd.startExam();
    }
}
