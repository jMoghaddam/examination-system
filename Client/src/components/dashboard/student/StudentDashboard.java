package components.dashboard.student;

import chat.ChatClient;
import chat.ExamChangeListener;
import components.dashboard.student.exams.StudentExamSection;
import components.dashboard.student.results.ExamResult;
import components.dashboard.teacher.chats.Chats;
import components.dashboard.teacher.settings.Settings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import logic.exam.Exam;
import logic.people.Participant;
import logic.people.Student;
import model.UserGetInfoHandler;
import utils.ControlledScreen;
import utils.ScreenController;

import java.io.File;
import java.io.IOException;

public class StudentDashboard implements ControlledScreen {

    public ImageView avatar;
    public Label name;
    public Label exams;
    public Label chats;
    public Label settings;
    public ImageView examsIcon;
    public ImageView chatsIcon;
    public ImageView settingsIcon;
    public HBox contentLayout;
    public VBox examsBox;
    public VBox chatsBox;
    public VBox settingsBox;
    private VBox examsLayout;
    private HBox chatsLayout;
    private VBox settingsLayout;
    private VBox examResultLayout;
    private StudentExamSection studentExamSection;
    private ExamResult examResult;
    private Chats chatSection;
    private Settings settingsSection;

    private Image exams_on;
    private Image exams_off;
    private Image chats_on;
    private Image chats_off;
    private Image settings_on;
    private Image settings_off;

    private ScreenController sc;
    private Student user;
    private String state = "exams";
    private ChatClient client;
    private UserGetInfoHandler userGetInfoHandler = new UserGetInfoHandler();

    @FXML
    public void initialize() {

        File file = new File("src/assets/images/exam_on.png");
        exams_on = new Image(file.toURI().toString());

        file = new File("src/assets/images/exam_off.png");
        exams_off = new Image(file.toURI().toString());

        file = new File("src/assets/images/chat_on.png");
        chats_on = new Image(file.toURI().toString());

        file = new File("src/assets/images/chat_off.png");
        chats_off = new Image(file.toURI().toString());

        file = new File("src/assets/images/settings_on.png");
        settings_on = new Image(file.toURI().toString());

        file = new File("src/assets/images/settings_off.png");
        settings_off = new Image(file.toURI().toString());

        examsBox.setOnMouseClicked((e) -> renderExams());

        chatsBox.setOnMouseClicked((e) -> renderChats());

        settingsBox.setOnMouseClicked((e) -> renderSettings());

    }

    @Override
    public void setScreen(ScreenController sc) {
        this.sc = sc;
    }

    public void setUser(Student user) {
        this.user = user;
        client.introduce(user.getStudentId());
        name.setText(user.getFirstName() + " " + user.getLastName());
        Image image = new Image(user.getAvatar().toURI().toString());
        avatar.setImage(image);
        renderExams();

        client.setExamChangeListener(examId -> {
            for (Exam e : user.getExams()) {
                if (e.getId().toString().equals(examId)) {
                    userGetInfoHandler.updateExam(e, user);
                }
            }
        });
    }

    private void offCurrentPanel() {

        switch (state) {
            case "exams" -> {
                examsBox.setStyle("-fx-background-color: #fff;" + "-fx-background-radius: 10%");
                examsBox.setPrefWidth(120);
                examsBox.setPrefHeight(105);
                examsIcon.setImage(exams_off);
                exams.setTextFill(Color.valueOf("#425C5A"));
                contentLayout.getChildren().removeAll(examsLayout);
            }
            case "chats" -> {
                chatsBox.setStyle("-fx-background-color: #fff;" + "-fx-background-radius: 10%");
                chatsBox.setPrefWidth(120);
                chatsBox.setPrefHeight(105);
                chatsIcon.setImage(chats_off);
                chats.setTextFill(Color.valueOf("#425C5A"));
                contentLayout.getChildren().removeAll(chatsLayout);
            }
            case "settings" -> {
                settingsBox.setStyle("-fx-background-color: #fff;" + "-fx-background-radius: 10%");
                settingsBox.setPrefWidth(120);
                settingsBox.setPrefHeight(105);
                settingsIcon.setImage(settings_off);
                settings.setTextFill(Color.valueOf("#425C5A"));
                contentLayout.getChildren().removeAll(settingsLayout);
            }
            case "examResult" -> {
                examsBox.setStyle("-fx-background-color: #fff;" + "-fx-background-radius: 10%");
                examsBox.setPrefWidth(120);
                examsBox.setPrefHeight(105);
                examsIcon.setImage(exams_off);
                exams.setTextFill(Color.valueOf("#425C5A"));
                contentLayout.getChildren().removeAll(examResultLayout);
            }

        }

    }

    public void renderExams() {

        offCurrentPanel();
        state = "exams";

        examsBox.setStyle("-fx-background-color: #1BA098;" + "-fx-background-radius: 10%");
        examsBox.setPrefWidth(144);
        examsBox.setPrefHeight(126);
        examsIcon.setImage(exams_on);
        exams.setTextFill(Color.valueOf("#fff"));

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("exams/examsSection.fxml"));
            examsLayout = loader.load();
            studentExamSection = loader.getController();
            studentExamSection.setScreen(sc);
            studentExamSection.setSd(this);
            studentExamSection.setUserInfo(user);
            contentLayout.getChildren().add(examsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void renderExamResult(Exam exam) {

        offCurrentPanel();
        state = "examResult";

        examsBox.setStyle("-fx-background-color: #1BA098;" + "-fx-background-radius: 10%");
        examsBox.setPrefWidth(144);
        examsBox.setPrefHeight(126);
        examsIcon.setImage(exams_on);
        exams.setTextFill(Color.valueOf("#fff"));

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("results/examResult.fxml"));
            examResultLayout = loader.load();
            examResult = loader.getController();
            examResult.setSd(this);
            examResult.setP(user);
            examResult.setExam(exam);
            contentLayout.getChildren().add(examResultLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void renderChats() {

        offCurrentPanel();
        state = "chats";

        chatsBox.setStyle("-fx-background-color: #1BA098;" + "-fx-background-radius: 10%");
        chatsBox.setPrefWidth(144);
        chatsBox.setPrefHeight(126);
        chatsIcon.setImage(exams_on);
        chats.setTextFill(Color.valueOf("#fff"));

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../teacher/chats/chats.fxml"));
            chatsLayout = loader.load();
            chatSection = loader.getController();
            chatSection.setClient(client);
            chatSection.setUser(user);
            contentLayout.getChildren().add(chatsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void renderSettings() {

        offCurrentPanel();
        state = "settings";

        settingsBox.setStyle("-fx-background-color: #1BA098;" + "-fx-background-radius: 10%");
        settingsBox.setPrefWidth(144);
        settingsBox.setPrefHeight(126);
        settingsIcon.setImage(exams_on);
        settings.setTextFill(Color.valueOf("#fff"));

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../teacher/settings/settings.fxml"));
            settingsLayout = loader.load();
            settingsSection = loader.getController();
            settingsSection.setUser(user);
            settingsSection.setFather(this);
            contentLayout.getChildren().add(settingsLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void takeExam(Exam exam) {

        for (Participant p : exam.getParticipants()) {
            if (p.getStudentId().equals(user.getStudentId())) {
                sc.setParticipatingScreen(exam, p, user);
                break;
            }
        }

    }

    public void update() {
        name.setText(user.getFirstName() + " " + user.getLastName());
        Image iv = new Image(user.getAvatar().toURI().toString());
        avatar.setImage(iv);
        renderExams();
    }

    public void logout(MouseEvent mouseEvent) {
        sc.setLoginScreen();
    }

    public void setClient(ChatClient client) {
        this.client = client;
    }
}
