package components.login;

import exceptions.WrongUsernameOrPasswordException;
import javafx.animation.FadeTransition;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import logic.people.Person;
import logic.people.Teacher;
import model.RegisterHandler;
import utils.ControlledScreen;
import utils.ScreenController;


public class LoginController implements ControlledScreen {

    public PasswordField password;
    public TextField username;
    public Button login;
    public Button signup;
    public Label errorDialog;

    private final RegisterHandler rh = RegisterHandler.getInstance(); // Handle Login Request
    private ScreenController sc; // Screen Transition

    @FXML
    public void initialize() {
        errorDialog.setVisible(false); // by default error dialog is hidden
    }

    // Go to Signup Screen
    public void onSignupClick(ActionEvent actionEvent) {
        sc.setSignupScreen();
    }

    // Login User
    public void onLoginClick(ActionEvent actionEvent) {

        // Extracting User Inputs
        String uname = username.getText();
        String pword = password.getText();

        // Required Fields
        if (uname.equals("")) {
            username.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);
        }
        if (pword.equals("")) {
            password.pseudoClassStateChanged(PseudoClass.getPseudoClass("error"), true);
            return;
        }

        // Go to Dashboard
        try {

            Person user = rh.login(uname, pword);

            if (user instanceof Teacher) {
                sc.setTeacherDashboardScreen(user);
            } else {
                sc.setStudentDashboardScreen(user);
            }

        } catch (WrongUsernameOrPasswordException e) {
            alert("نام کاربری یا رمز عبور اشتباه است");
        }

    }

    private void alert(String message) {

        // Set Toast Message
        errorDialog.setText(message);

        // Fade in Fade out Animation
        FadeTransition fadeIn = new FadeTransition(
                Duration.millis(2000)
        );
        fadeIn.setNode(errorDialog);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.setCycleCount(2);
        fadeIn.setAutoReverse(true);
        fadeIn.setOnFinished(actionEvent -> errorDialog.setVisible(false));

        if (!errorDialog.isVisible()) {
            errorDialog.setVisible(true);
            fadeIn.playFromStart();
        }

    }

    @Override
    public void setScreen(ScreenController sc) {

        System.out.println("login");
        this.sc = sc;
    }
}
