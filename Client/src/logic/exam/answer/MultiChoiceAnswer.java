package logic.exam.answer;

import logic.exam.question.Choice;

public class MultiChoiceAnswer extends Answer {

    private Choice answer;

    public Choice getAnswer() {
        return answer;
    }

    public void setAnswer(Choice answer) {
        this.answer = answer;
    }
}
