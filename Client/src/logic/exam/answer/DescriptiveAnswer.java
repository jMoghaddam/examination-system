package logic.exam.answer;

import java.io.File;

public class DescriptiveAnswer extends Answer {

    private String text;
    private File image;
    private File file;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
