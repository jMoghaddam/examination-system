package logic.exam;

public class Result {

    private double avg;
    private int graded;
    private int isSimple;
    private int isNormal;
    private int isHard;

    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

    public int getGraded() {
        return graded;
    }

    public void setGraded(int graded) {
        this.graded = graded;
    }

    public void gradedInc() {
        this.graded++;
    }


    public int getIsSimple() {
        return isSimple;
    }

    public void setIsSimple(int isSimple) {
        this.isSimple = isSimple;
    }

    public void simpleInc() {
        this.isSimple++;
    }

    public int getIsNormal() {
        return isNormal;
    }

    public void setIsNormal(int isNormal) {
        this.isNormal = isNormal;
    }

    public void normalInc() {
        this.isNormal++;
    }

    public int getIsHard() {
        return isHard;
    }

    public void setIsHard(int isHard) {
        this.isHard = isHard;
    }

    public void hardInc() {
        this.isHard++;
    }
}
