package logic.exam.question;

import java.util.ArrayList;

public class MultiChoiceQuestion extends Question {

    private ArrayList<Choice> choices;
    private Choice correctAnswer;

    public ArrayList<Choice> getChoices() {
        return choices;
    }

    public void setChoices(ArrayList<Choice> choices) {
        this.choices = choices;
    }

    public Choice getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(Choice correctAnswer) {
        this.correctAnswer = correctAnswer;
    }
}
