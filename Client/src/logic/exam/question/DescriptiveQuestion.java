package logic.exam.question;

public class DescriptiveQuestion extends Question {

    private boolean imageAllowed;
    private boolean fileAllowed;

    public boolean isImageAllowed() {
        return imageAllowed;
    }

    public void setImageAllowed(boolean imageAllowed) {
        this.imageAllowed = imageAllowed;
    }

    public boolean isFileAllowed() {
        return fileAllowed;
    }

    public void setFileAllowed(boolean fileAllowed) {
        this.fileAllowed = fileAllowed;
    }

}
