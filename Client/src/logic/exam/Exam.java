package logic.exam;

import logic.Date;
import logic.Time;
import logic.chat.Group;
import logic.exam.question.Question;
import logic.people.Participant;
import logic.people.Person;
import org.bson.types.ObjectId;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Exam {

    private ObjectId id;
    private String title;
    private boolean isReviewable;
    private Date startDate;
    private Time startTime;
    private Time duration;
    private ArrayList<Question> questions = new ArrayList<>();
    private ArrayList<Participant> participants = new ArrayList<>();
    private Result result = new Result();
    private Group group;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isReviewable() {
        return isReviewable;
    }

    public void setReviewable(boolean reviewable) {
        isReviewable = reviewable;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getDuration() {
        return duration;
    }

    public void setDuration(Time duration) {
        this.duration = duration;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public ArrayList<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<Participant> participants) {
        this.participants = participants;
    }

    public void setParticipant(Participant participant) {
        participants.add(participant);
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Person getUserById(String id) {

        for (Participant p : participants) {
            if (p.getStudentId().equals(id)) return p;
        }

        return null;
    }
}
