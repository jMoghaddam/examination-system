package logic;

import java.time.LocalDateTime;

public class Date {

    private int year;
    private int month;
    private int day;

    public Date(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public Date() {}

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public static int isBigger(Date d1, Date d2) {

        if (d1.getYear() > d2.getYear()) return 1;
        if (d1.getYear() < d2.getYear()) return -1;

        if (d1.getMonth() > d2.getMonth()) return 1;
        if (d1.getMonth() < d2.getMonth()) return -1;

        if (d1.getDay() > d2.getDay()) return 1;
        if (d1.getDay() < d2.getDay()) return -1;

        return 0;

    }

    public static Date toDate(String dateStr) {
        Date date = new Date();
        String[] strs = dateStr.split("/");
        date.setYear(Integer.parseInt(strs[0]));
        date.setMonth(Integer.parseInt(strs[1]));
        date.setDay(Integer.parseInt(strs[2]));
        return date;
    }

    @Override
    public String toString() {
        String yearStr = String.valueOf(getYear());
        String monthStr = getMonth() > 9 ? String.valueOf(getMonth()) : "0" + getMonth();
        String dayStr = getDay() > 9 ? String.valueOf(getDay()) : "0" + getDay();
        return yearStr + "/" + monthStr + "/" + dayStr;
    }
}
