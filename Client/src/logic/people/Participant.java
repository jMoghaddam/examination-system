package logic.people;

import logic.exam.answer.Answer;

import java.util.ArrayList;

public class Participant extends Student implements Comparable<Participant> {

    private boolean isBanned;
    private boolean hasParticipated;
    private ArrayList<Integer> questionQueue = new ArrayList<>();
    private ArrayList<Answer> answers;
    private double grade;
    private int rank;

    public boolean isBanned() {
        return isBanned;
    }

    public void setBanned(boolean banned) {
        isBanned = banned;
    }

    public boolean isHasParticipated() {
        return hasParticipated;
    }

    public void setHasParticipated(boolean hasParticipated) {
        this.hasParticipated = hasParticipated;
    }

    public ArrayList<Integer> getQuestionQueue() {
        return questionQueue;
    }

    public void setQuestionQueue(ArrayList<Integer> questionQueue) {
        this.questionQueue = questionQueue;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public int compareTo(Participant o) {
        if (this.getGrade() > o.getGrade()) return 1;
        else if (o.getGrade() > this.getGrade()) return -1;
        return 0;
    }
}
