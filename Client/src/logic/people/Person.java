package logic.people;

import logic.exam.Exam;
import org.bson.types.ObjectId;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public abstract class Person {

    private ObjectId id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private File avatar;
    private String bio;
    private String saveDirectoryPath;
    private ArrayList<Exam> exams = new ArrayList<>();

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public File getAvatar() {
        return avatar;
    }

    public void setAvatar(String path) {
        avatar = new File(path);
        try {
            avatar.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getSaveDirectoryPath() {
        return saveDirectoryPath;
    }

    public void setSaveDirectoryPath(String saveDirectoryPath) {
        this.saveDirectoryPath = saveDirectoryPath;
    }

    public void setAvatar(File avatar) {
        this.avatar = avatar;
    }

    public ArrayList<Exam> getExams() {
        return exams;
    }

    public void setExams(ArrayList<Exam> exams) {
        this.exams = exams;
    }
}
