package logic;

public class Time {

    int hour;
    int min;
    int sec = 0;

    public static Time toTime(long duration) {

        Time time = new Time();
        time.setHour((int) (duration / 3600));
        time.setMin((int) (duration - (time.getHour() * 3600)) / 60);
        return time;

    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getSec() {
        return sec;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }

    public boolean minus() {

        if (sec > 0) sec--;
        else {
            if (min > 0) min --;
            else if (hour > 0) {
                hour--;
            } else {
                return false;
            }
        }

        return true;

    }

    public static int isBigger(Time t1, Time t2) {

        if (t1.getHour() > t2.getHour()) return 1;
        if (t1.getHour() < t2.getHour()) return -1;

        if (t1.getMin() > t2.getMin()) return 1;
        if (t1.getMin() < t2.getMin()) return -1;

        return 0;

    }

    public String toVeryString() {
        String hourString = getHour() > 9 ? String.valueOf(getHour()) : "0" + getHour();
        String minString = getMin() > 9 ? String.valueOf(getMin()) : "0" + getMin();
        String secString = getSec() > 9 ? String.valueOf(getSec()) : "0" + getSec();
        return hourString + ":" + minString + ":" + secString;
    }

    @Override
    public String toString() {
        String hourString = getHour() > 9 ? String.valueOf(getHour()) : "0" + getHour();
        String minString = getMin() > 9 ? String.valueOf(getMin()) : "0" + getMin();
        return hourString + ":" + minString;
    }

    public static Time sToTime(long s) {

        Time time = new Time();
        time.setHour((int) (s / 3600));
        s -= time.getHour() * 3600;
        time.setMin((int) s / 60);
        s -= time.getMin() * 60;
        time.setSec((int) s);

        return time;

    }

    public static Time toDate(String dateStr) {

        Time time = new Time();

        if (dateStr.equals("")) {
            return null;
        }

        String[] pieces = dateStr.split(":");
        time.setHour(Integer.parseInt(pieces[0]));
        time.setMin(Integer.parseInt(pieces[1]));
        return time;
    }

    public long toMillis() {
        return (hour * 60 * 60) + (min * 60);
    }

}
