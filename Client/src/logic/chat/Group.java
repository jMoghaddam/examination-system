package logic.chat;

import logic.people.Person;

import java.util.ArrayList;

public class Group {

    private String hash;
    private ArrayList<Member> members = new ArrayList<>();
    private ArrayList<Message> messages = new ArrayList<>();

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public ArrayList<Member> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<Member> members) {
        this.members = members;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    public Member getMemberById(String id) {

        for (Member m : members) {
            if (m.getId().equals(id)) {
                return m;
            }
        }

        return null;

    }
}
