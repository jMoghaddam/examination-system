package chat;

public interface MessageListener {
    void newMessage(String hash, String from, String time, String text);
}
