package chat;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;
import logic.Time;
import org.bson.types.ObjectId;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ChatClient {

    private final String serverName;
    private final int port;
    private Socket connection;
    private InputStreamReader inputStreamReader;
    private BufferedReader bufferedReader;
    private PrintWriter printWriter;
    private UserStatusListener userStatusListener;
    private ExamChangeListener examChangeListener;
    private MessageListener messageListener;

    public ChatClient(String serverName, int port) {
        this.serverName = serverName;
        this.port = port;
    }

    public void setUserStatusListener(UserStatusListener userStatusListener) {
        this.userStatusListener = userStatusListener;
    }

    public void setExamChangeListener(ExamChangeListener examChangeListener) {
        this.examChangeListener = examChangeListener;
    }

    public void setMessageListener(MessageListener messageListener) {
        this.messageListener = messageListener;
    }

    public void connect() {

        try {

            this.connection = new Socket(serverName, port);
            inputStreamReader = new InputStreamReader(connection.getInputStream());
            bufferedReader = new BufferedReader(inputStreamReader);
            printWriter = new PrintWriter(connection.getOutputStream());
            startMessageReader();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void startMessageReader() {

        Thread t = new Thread(() -> {
            try {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    System.out.println(line);
                    String[] tokens = line.split(" ", 2);
                    switch (tokens[0]) {
                        case "updateExam" -> updateExam(tokens[1]);
                        case "msg" -> newMessage(tokens[1]);
                        case "online" -> putOnline(tokens[1]);
                        case "offline" -> putOffline(tokens[1]);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        t.start();

    }

    private void putOffline(String token) {
        userStatusListener.offline(token);
    }

    private void putOnline(String token) {
        userStatusListener.online(token);
    }

    private void newMessage(String token) {
        String[] tokens = token.split(" ", 5);
        String hash = tokens[0];
        String from = tokens[1];
        String time = tokens[2] + " " + tokens[3];
        String text = tokens[4];
        messageListener.newMessage(hash, from, time, text);
    }

    private void updateExam(String token) {
        examChangeListener.examChanged(token);
    }

    public void send(String msg) {
        printWriter.println(msg);
        printWriter.flush();
    }

    public void introduce(String id) {
        printWriter.println("introduce " + id);
        printWriter.flush();
    }
}
