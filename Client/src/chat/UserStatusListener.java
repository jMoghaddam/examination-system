package chat;

public interface UserStatusListener {
    void online(String id);
    void offline(String id);
}
