package chat;

public interface ExamChangeListener {
    void examChanged(String examId);
}
