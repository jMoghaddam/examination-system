package exceptions;

public class WrongUsernameOrPasswordException extends RuntimeException {

    public WrongUsernameOrPasswordException() {
        super("WrongUsernameOrPassword");
    }

}
