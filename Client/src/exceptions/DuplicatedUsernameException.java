package exceptions;

public class DuplicatedUsernameException extends RuntimeException {

    public DuplicatedUsernameException() {
        super("WrongUsernameOrPassword");
    }

}
