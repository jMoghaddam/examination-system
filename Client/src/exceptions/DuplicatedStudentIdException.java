package exceptions;

public class DuplicatedStudentIdException extends RuntimeException {

    public DuplicatedStudentIdException() {
        super("DuplicatedStudentIdException");
    }

}
