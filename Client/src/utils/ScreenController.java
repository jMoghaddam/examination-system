package utils;

import chat.ChatClient;
import components.dashboard.student.StudentDashboard;
import components.dashboard.student.participating.Participating;
import components.dashboard.teacher.TeacherDashboard;
import components.dashboard.teacher.grading.Grading;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import logic.exam.Exam;
import logic.people.Participant;
import logic.people.Person;
import logic.people.Student;
import logic.people.Teacher;
import model.UserGetInfoHandler;

import java.io.IOException;
import java.util.HashMap;

// We Use StackPane as the very root layout of the scene to make transition between layouts easier
public class ScreenController extends StackPane {

    // Store all the Screens
    private final HashMap<String, Node> screens = new HashMap<>();
    private ChatClient client;
    private UserGetInfoHandler userGetInfoHandler = new UserGetInfoHandler();

    public ScreenController() {
        super();
    }

    // Showing Screen to the User with Animation
    public void setScreen(String name) {

        // Check if Screen is loaded
        if (screens.get(name) != null) {
            final DoubleProperty opacity = opacityProperty();

            // Check if StackPane is Empty
            if (!getChildren().isEmpty()) {

                // Fade out Current Screen
                Timeline fade = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
                        new KeyFrame(new Duration(1), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                // Set Screen
                                getChildren().remove(0);
                                getChildren().add(0, screens.get(name));

                                // Fade in new Screen
                                Timeline fadeIn = new Timeline(
                                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                                        new KeyFrame(new Duration(500), new KeyValue(opacity, 1.0))
                                );
                                fadeIn.play();
                            }
                        }, new KeyValue(opacity, 0.0))
                );
                fade.play();

            } else {

                // Set Initial Screen
                setOpacity(0.0);
                getChildren().add(screens.get(name));
                Timeline fadeIn = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                        new KeyFrame(new Duration(2500), new KeyValue(opacity, 1.0))
                );
                fadeIn.play();

            }

        }
    }

    // Set Login Screen
    public void setLoginScreen() {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("../components/login/login.fxml"));
            Parent loadScreen = loader.load();
            ControlledScreen screenController = (ControlledScreen) loader.getController();
            screenController.setScreen(this);
            if (!screens.containsKey("login")) screens.put("login", loadScreen);
            setScreen("login");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Set Signup Screen
    public void setSignupScreen() {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("../components/signup/signup.fxml"));
            Parent loadScreen = loader.load();
            ControlledScreen screenController = (ControlledScreen) loader.getController();
            screenController.setScreen(this);
            if (!screens.containsKey("signup")) screens.put("signup", loadScreen);
            setScreen("signup");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Set Teacher's Dashboard Screen
    public void setTeacherDashboardScreen(Person user) {

        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("../components/dashboard/teacher/teacherDashboard.fxml"));
            Parent loadScreen = loader.load();
            if (!screens.containsKey("teacherDashboard")) screens.put("teacherDashboard", loadScreen);
            TeacherDashboard td = (TeacherDashboard) loader.getController();
            td.setClient(client);
            td.setScreen(this);
            td.setUser((Teacher) user);
            setScreen("teacherDashboard");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setGradingScreen(Exam exam, Teacher user) {

        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("../components/dashboard/teacher/grading/grading.fxml"));
            Parent loadScreen = loader.load();
            if (!screens.containsKey("grading")) screens.put("grading", loadScreen);
            Grading g = (Grading) loader.getController();
            g.setScreen(this);
            g.setUser(user);
            g.setExam(exam);
            setScreen("grading");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setStudentDashboardScreen(Person user) {

        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("../components/dashboard/student/studentDashboard.fxml"));
            Parent loadScreen = loader.load();
            screens.put("studentDashboard", loadScreen);
            StudentDashboard sd = (StudentDashboard) loader.getController();
            sd.setClient(client);
            sd.setScreen(this);
            sd.setUser((Student) user);
            setScreen("studentDashboard");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setParticipatingScreen(Exam exam, Participant participant, Student user) {

        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("../components/dashboard/student/participating/participating.fxml"));
            Parent loadScreen = loader.load();
            screens.put("participating", loadScreen);
            Participating g = loader.getController();
            g.setScreen(this);
            g.setClient(client);
            g.setInfo(exam, participant);
            g.setUser(user);
            setScreen("participating");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setClient(ChatClient client) {
        this.client = client;
    }
}
