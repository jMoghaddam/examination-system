package utils;

// Easy Transition between Screens
public interface ControlledScreen {

    void setScreen(ScreenController screenPage);

}
