package utils;

import logic.exam.Exam;
import logic.people.Participant;
import logic.people.Teacher;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

public class ExcelHandler {

    public ArrayList<Participant> setParticipants(File excel) {

        ArrayList<Participant> participants = new ArrayList<>();
        int currentCell = 0;

        try {

            FileInputStream fip = new FileInputStream(excel);
            XSSFWorkbook workbook = new XSSFWorkbook(fip);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();
            DataFormatter fmt = new DataFormatter();

            while (rows.hasNext()) {
                currentCell = 0;
                Participant participant = new Participant();

                XSSFRow row = (XSSFRow) rows.next();
                Iterator<Cell> cells = row.cellIterator();

                while (cells.hasNext()) {
                    currentCell++;
                    Cell cell = cells.next();

                    switch (currentCell) {
                        case 1 -> {
                            String value = fmt.formatCellValue(cell);
                            if (! value.trim().isEmpty()) {
                                participant.setStudentId(value);
                            }
                        }
                        case 2 -> participant.setFirstName(cell.getStringCellValue());
                        case 3 -> participant.setLastName(cell.getStringCellValue());
                    }

                }
                participants.add(participant);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return participants;


    }

    public File generateExamResult(Exam exam, Teacher user) {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("results");
        XSSFRow row;
        FileOutputStream fileOutputStream = null;
        int rowid = 0;
        try {

            for (Participant p : exam.getParticipants()) {

                if (!Double.isNaN(p.getGrade())) {

                    row = sheet.createRow(rowid++);

                    Cell cell1 = row.createCell(0);
                    cell1.setCellValue(p.getStudentId());

                    Cell cell2 = row.createCell(1);
                    cell2.setCellValue(p.getGrade());

                }

            }

            File generate = new File(user.getSaveDirectoryPath() + "/" + exam.getTitle() + ".xlsx");
            fileOutputStream = new FileOutputStream(generate);
            workbook.write(fileOutputStream);
            fileOutputStream.close();

            return generate;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public File generateExamsResult(ArrayList<Exam> exams, Teacher user) {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("results");
        XSSFRow row;
        FileOutputStream fileOutputStream = null;
        int rowid = 0;
        try {

            for (Exam e : exams) {

                if (e.getResult().getGraded() == e.getParticipants().size() * e.getQuestions().size()) {

                    row = sheet.createRow(rowid++);

                    Cell cell1 = row.createCell(0);
                    cell1.setCellValue(e.getTitle());

                    Cell cell2 = row.createCell(1);
                    cell2.setCellValue(e.getResult().getAvg());

                }

            }

            File generate = new File(user.getSaveDirectoryPath() + "/generalResults.xlsx");
            fileOutputStream = new FileOutputStream(generate);
            workbook.write(fileOutputStream);
            fileOutputStream.close();

            return generate;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
