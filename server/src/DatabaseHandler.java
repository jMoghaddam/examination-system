import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import java.util.ArrayList;

public class DatabaseHandler {

    MongoClient mongoClient = new MongoClient();
    MongoDatabase database = mongoClient.getDatabase("Examiner"); // database
    MongoCollection<Document> examsDB = database.getCollection("exams"); // exams collection
    MongoCollection<Document> studentsDB = database.getCollection("Students"); // students collection


    public ArrayList<String> getParticipants(String token) {

        ArrayList<String> ids = new ArrayList<>();
        Document query = new Document("_id", new ObjectId(token));
        ArrayList<Document> pdocs = (ArrayList<Document>) examsDB.find(query).first().get("participants");

        for (Document pdoc : pdocs) {
            Document q2 = new Document("studentId", pdoc.getString("studentId"));
            ObjectId id = studentsDB.find(q2).first().getObjectId("_id");
            System.out.println(id);
            ids.add(id.toString());
        }

        return ids;

    }

    public ArrayList<String> saveMessage(String hash, String from, String time, String text) {

        Document query = new Document("group.hash", hash);
        Document message = new Document("from", from);
        message.append("time", time);
        message.append("text", text);
        Document f = new Document("group.messages", message);
        Document update = new Document("$addToSet", f);
        examsDB.updateOne(query, update);

        Document g = (Document) examsDB.find(query).first().get("group");
        ArrayList<Document> mms = (ArrayList<Document>) g.get("members");
        ArrayList<String> ids = new ArrayList<>();
        for (Document m : mms) {
            ids.add(m.getString("id"));
        }

        return ids;
    }

    public void setStatus(String id, int status) {

        Document query = new Document("group.members.id", id);
        Document f = new Document("group.members.$.status", status);
        Document update = new Document("$set", f);
        examsDB.updateMany(query, update);

    }

}
