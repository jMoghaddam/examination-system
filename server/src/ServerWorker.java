import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class ServerWorker extends Thread {

    private final Server server;
    private final Socket connection;
    private InputStreamReader inputStreamReader;
    private BufferedReader bufferedReader;
    private PrintWriter printWriter;
    private String id;
    private DatabaseHandler databaseHandler = new DatabaseHandler();

    public ServerWorker(Server server, Socket connection) {
        this.server = server;
        this.connection = connection;
    }

    @Override
    public void run() {

        try {
            handleConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void handleConnection() throws IOException {

        inputStreamReader = new InputStreamReader(connection.getInputStream());
        bufferedReader = new BufferedReader(inputStreamReader);
        printWriter = new PrintWriter(connection.getOutputStream());

        String line;
        try {
            while ( (line = bufferedReader.readLine()) != null ) {

                System.out.println(line);
                String[] tokens = line.split(" ", 2);
                switch (tokens[0]) {
                    case "introduce" -> introduce(tokens[1]);
                    case "updateExam" -> updateExam(tokens[1]);
                    case "msg" -> handleSMessage(tokens[1]);
                }

            }
        } catch (IOException e) {
            System.out.println("bye bye " + connection);
        }

        for (ServerWorker worker : server.getWorkers()) {
            worker.send("offline " + id);
        }

        databaseHandler.setStatus(id, 0);
        server.getWorkers().remove(this);
        connection.close();

    }

    private void handleSMessage(String token) {

        String[] tokens = token.split(" ", 3);
        String hash = tokens[0];
        String from = tokens[1];
        String text = tokens[2];
        LocalDateTime now = LocalDateTime.now();
        String time = now.getYear() + "/" + now.getMonth() + "/" + now.getDayOfMonth() + " " + now.getHour() + ":" + now.getMinute();
        ArrayList<String> ids = databaseHandler.saveMessage(hash, from, time, text);

        for (ServerWorker worker : server.getWorkers()) {
            for (String id : ids) {
                if (worker.id.equals(id)) {
                    worker.send("msg " + hash + " " + from + " " + time + " " + text);
                    break;
                }
            }
        }

    }

    private void updateExam(String token) {

        ArrayList<String> ids = databaseHandler.getParticipants(token);
        for (ServerWorker worker : server.getWorkers()) {
            if (ids.contains(worker.id)) {
                worker.send("updateExam " + token);
            }
        }

    }

    public void send(String msg) {
        printWriter.println(msg);
        printWriter.flush();
    }

    private void introduce(String id) {
        this.id = id;
        databaseHandler.setStatus(id, 1);

        for (ServerWorker worker : server.getWorkers()) {
            if (!worker.id.equals(id)) {
                worker.send("online " + id);
            }
        }

    }

}
