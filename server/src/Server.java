import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server extends Thread {

    private final int port;
    private ArrayList<ServerWorker> workers = new ArrayList<>();

    public Server(int port) {
        this.port = port;
    }

    public ArrayList<ServerWorker> getWorkers() {
        return workers;
    }

    @Override
    public void run() {

        try {
            ServerSocket ss = new ServerSocket(port);
            while (true) {
                Socket connection = ss.accept();
                System.out.println("Accepted Connection from " + connection);
                ServerWorker worker = new ServerWorker(this, connection);
                workers.add(worker);
                worker.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
